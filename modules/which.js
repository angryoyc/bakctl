/*
*
*   Тестирование системных утилит на наличие
*
*/

let whichutil = null;
const exec = require('./exec');
const fs = require( 'fs' );
const { promisify } = require( 'util' );
const stat = promisify( fs.stat );
let dirs = [ '/usr/bin', '/bin', '/usr/local/bin', '/usr/local/sbin', '/usr/sbin', '/sbin' ];
module.exports = async function sys_utils_check( list, required ){
	const result = {};
	if( !whichutil ){
		whichutil = await manual_which( 'which' );
	}
	if( whichutil ){
		for( let u of list ){
			let a;
			try{
				a = await exec( whichutil, [ u ] );
				result[ u ] = a[ 0 ];
			}catch(e){
				if(e.code == 1){
					let fpath = await manual_which( u );
					if( fpath ){
						result[ u ] = fpath ;
					}else if( required ){
						throw new Error('Не могу обнаружить ' + u);
					}
				}else{
					throw e;
				}
			}
		}
		return result;
	}else{
		throw new Error('Не могу найти утилиту which');
	}
};

async function manual_which( name ){
	let path_dirs = ( process.env['PATH'] || '' ).split( /:/ ).concat( dirs );
	for( let d of path_dirs ){
		try{
			let fpath = d + '/' + name;
			await stat( fpath );
			return fpath;
		}catch(e){noop();}
	}
	return null;
}

function noop(){}