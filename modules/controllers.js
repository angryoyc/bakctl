const cor         = new (require('../lib/core'))(__dirname + '/../config.json');

const { print_object }    = require('../modules/print_methods');

exports.search = async (req, res , params) => {
	return new Promise(async (resolve, reject) => {
		try{
			const resp_data = [{ 'text' :'upper_25', 'value': 1}, { 'text' :'upper_75', 'value': 2}];
			console.log(resp_data);
			res.end(JSON.stringify(resp_data));
			resolve();
		}catch(e){
			reject(e);
		};
	});
};



exports.query = async (req, res , params) => {
	return new Promise(async (resolve, reject) => {
		try{
			const resp_data = {};
			console.log(resp_data);
			res.end(JSON.stringify(resp_data));
			resolve();
		}catch(e){
			reject(e);
		};
	});
};

exports.annotations = async (req, res , params) => {
	return new Promise(async (resolve, reject) => {
		try{
			const resp_data = {};
			console.log(resp_data);
			res.end(JSON.stringify(resp_data));
			resolve();
		}catch(e){
			reject(e);
		};
	});
};



exports.list = async (req, res , params) => {
	return new Promise(async (resolve, reject) => {
		try{
			let list = await cor.task_details( { position: 'all' } );
			res.setHeader('Content-Type', 'text/json; charset=utf-8');
			print_object(list);
			res.end(JSON.stringify(
				list.map(t=>{
					return {
						id: t.id,
						group: t.group,
						checkmd5: t.checkmd5,
						laststate: t.laststate,
						message: t.message,
						dst:{
							local: t.dst.local,
							proto: t.dst.proto,
							host: t.dst.host,
							basename: t.dst.basename,
							dirname: t.dst.dirname
						}
					}
				})
			));
			resolve();
		}catch(e){
			reject(e);
		};
	});
};
