'use strict';

module.exports = function (c) {

	const pg = require('pg');
	let client = null;
	let ready = false;
	let conf = c;
	let trycounter;

	this.sql = async function (sql, values, for_all_results, for_each_row) {
		return new Promise(async (resolve, reject) => {
			try {
				let lasterror;
				trycounter = 0;
				while ((!ready) && (trycounter++ < 5)) {
					try { await connect(); } catch (err) {
						lasterror = err;
					}
				}
				if (!ready && (!((lasterror || { message: '' }).message || '').match(/already been connected/))) {
					reject(lasterror || (new Error('Can\'t connect to database')));
				} else {
					client.query(sql, values, async function (err, result) {
						if (err) {
							reject(err);
						} else {
							if (typeof (for_all_results) == 'function') {
								await for_all_results(result);
							}
							if (typeof (for_each_row) == 'function') {
								for (let row of result.rows) {
									await for_each_row(row);
								}
							}
							resolve(result);
						}
					});
				}
			} catch (err) {
				reject(err);
			}
		});
	};

	this.close = function () {
		if (client) client.end();
	};

	this.info = async function () {
		return { connectionstring: conf.connectionstring, ready: ready };
	};

	function onclienterror(err) {
		ready = false;
		client.end();
		client = new pg.Client(conf.connectionstring);
		client.on('error', onclienterror);
		process.stdout.write(err.message + '\n');
	}

	async function connect() {
		return new Promise(async (resolve, reject) => {
			try {
				client = client || new pg.Client(conf.connectionstring);
				let onerror = function (err) {
					if (err) {
						ready = false;
						if (err.code === 'ECONNREFUSED') {
							trycounter++;
							process.stdout.write('Connection refused. Tring again...' + '\n');
							client.end();
							client = new pg.Client(conf.connectionstring);
							client.on('error', onclienterror);
							setTimeout(() => {
								reject(err);
							}, 2000);
						} else {
							reject(err);
						}
					} else {
						ready = true;
						resolve();
					}
				};
				client.connect(onerror);
			} catch (err) {
				reject(err);
			}
		});
	}
};
