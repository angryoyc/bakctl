




module.exports = function stringifyUrl( urlobj, showpassword ){
	return (
		prefix( urlobj.proto ) +
		auth( urlobj, showpassword ) +
		(urlobj.host?urlobj.host:'') +
		(urlobj.port?(':'+urlobj.port):'') +
		urlobj.dirname + '/' +
		urlobj.basename
	);
}


/*
module.exports = function stringifyUrl( urlobj, showpassword ){
	if( urlobj.local ){
		return (
			urlobj.dirname + '/' +
			urlobj.basename
		);
	}else{
		return (
			prefix( urlobj.proto ) +
			(('user' in urlobj)?(urlobj.user + ((showpassword && urlobj.pass)?':'+urlobj.pass:'') + '@'):'') +
			urlobj.host + ':' +
			urlobj.port +
			urlobj.dirname + '/' +
			urlobj.basename
		);
	}
}
*/

function prefix( proto ){
	if( proto =='ssh'){
		return 'ssh://';
	}else if( (proto =='pgsql') || (proto == 'postgres') ){
		return 'pgsql://';
	}else{
		return proto + '://';
	}
}
function auth( urlobj, showpassword ){
	if( ('user' in urlobj) && (urlobj.user) ){
		return (urlobj.user + ((showpassword && urlobj.pass)?':'+urlobj.pass:'') + '@');
	}else{
		return '';
	}
}