/*
*
* parse .pgpass
* like this
*
* 127.0.0.1:5432:*:postgres:oblako
*
*/

const fs = require('fs');
const { promisify } = require('util');
const writeFile = promisify(fs.writeFile);
const readFile = promisify(fs.readFile);
const homedir = require('os').homedir();
const exec = require('../modules/exec');

let body = [];

exports.load = async function(){
	let strbody;
	try{
		strbody = await readFile( homedir + '/' + '.pgpass');
	}catch(err){
		strbody = '';
	}
	body = strbody.toString('utf8')
	.split(/\n/)
	.map(l=>{
		return l.toString().trim();
	})
	.filter(l=>{
		return l;
	})
	.map(l=>{
		let a = l.split(/:/);
		return {
			host:a[0],
			port:a[1],
			base:a[2],
			user:a[3],
			pass:a[4]
		};
	});
	return;
}

exports.save = async function(){
	await writeFile(homedir + '/' + '.pgpass', body
		.map(l=>{
			return l.host + ':' + l.port + ':' + l.base + ':' + l.user + ':' + l.pass;
		})
		.join('\n') + '\n'
	);
	await exec('chmod', ['600', homedir + '/' + '.pgpass']);
}

exports.find = function(host, port, base, user){
	for( let rec of body ){
		if(
			(rec.host == host)
			&&
			(rec.port == port)
			&&
			( (rec.base == base) || (rec.base == '*') )
			&&
			(rec.user == user)
		){
			return true;
		}
	}
	return false;
}

exports.add = function( host, port, base, user, pass ){
	body.push({
		'host': host,
		'port': port,
		'base': base,
		'user': user,
		'pass': pass
	});
}
