module.exports = function ( conf, position ){
	let list;
	if( position == 'all' ){
		list = conf.objects;
	}else if(position == 'last'){
		if( conf.objects.length>0 ){
			list = [ conf.objects[conf.objects.length -1] ];
		}else{
			throw new Error('Список пуст');
		}
	}else{
		let i;
		if( (position.length==6) && (position.match(/[0-9]/)) && (position.match(/[a-z_.]/)) ){
			// id
			for( i = 0; i<conf.objects.length; i++ ) if( conf.objects[i].id == position ) break;
		}else if( position.match(/^\d+$/) ){
			// num
			i = parseInt( position ) - 1;
		}
		if( typeof(i) != 'undefined' ){
			if( (i>=0) && (conf.objects.length > i) ){
				list = [ conf.objects[i] ];
			}else{
				throw new Error('Указана позиция за границами списка');
			}
		}else{
			list = [];
			for( i = 0; i<conf.objects.length; i++ ){
				if( (conf.objects[i].group || '') == position ) list.push( conf.objects[i] );
			}
		}
	}
	return list;
}