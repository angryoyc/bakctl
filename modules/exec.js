const spawn = require('child_process').spawn;

module.exports = async function exec( binname, params, stdindata, options){
	return new Promise(( resolve, reject )=>{
		const bin = spawn( binname, params, options || {} );
		const stderr_data = [];
		const stdout_data = [];
		bin.stdout.on('data',  data => { stdout_data.push(data.toString('utf8')); });
		bin.stderr.on('data',  data => { stderr_data.push(data.toString('utf8')); });
		bin.on('close', code => {
			let a, b;
			if( code ){
				//error
				b = stdout_data.join('').split(/\r*\n/).filter( l=>{ return l;} );
				a = stderr_data.join('').split(/\r*\n/).filter( l=>{ return l;} );
				let err;
				if(a.length>0 || b.length>0){
					err = new Error(a[0]);
					err.rows = (a || []).concat(b || []);
				}else{
					err  = new Error('Неизвестная ошибка');
				}
				err.code = code;
				reject(err);
			}else{
				//ok
				a = stdout_data.join('').split(/\r*\n/).filter( l=>{ return l;} );
				resolve( a );
			}
		});
		if(stdindata) bin.stdin.end(stdindata);
	});
}
