
/**
* Печать таблицы
* @param {Object} header - объект, описывающий колонки - их ширину заголовки, тип данных в них и т.п.
* @param {Object} rows   - строки для вывода в формате, возвращаемом SQL-запросом (pdb.sql)
* @param {Integer} idots - число, определящие вывод точек (и т.д.) под шапкой или в конце таблицы 0
*                          0 - не выводить; 1 - после заголовка; 2 - в конце; 3 - в обоих случаях;
*/

const printws = require('../modules/printws');

module.exports = function table(header, rows, idots){
	const dots = idots || 0;
	const N = 20;

	for(let h of header){
		process.stdout.write( '-' );
		process.stdout.write( '-'.repeat( h.width || N ) );
		if(header[header.length-1].column!=h.column) process.stdout.write( '-+' );
	}
	process.stdout.write( '\n' );
	for(let h of header){
		process.stdout.write( ' ' );
		process.stdout.write( printws(h.title, ( h.width || N )) );
		if(header[header.length-1].column!=h.column) process.stdout.write( ' |' );
	}
	process.stdout.write( '\n' );
	for(let h of header){
		process.stdout.write( '-' );
		process.stdout.write( '-'.repeat( h.width || N ) );
		if(header[header.length-1].column!=h.column) process.stdout.write( '-+' );
	}
	process.stdout.write( '\n' );

	if( ((dots & 1)==1) && (rows.length>0) ){
		for(let h of header){
			process.stdout.write( ' ' );
			process.stdout.write( printws( '...'.substr(0, h.title.length), ( h.width || N )) );
			if(header[header.length-1].column!=h.column) process.stdout.write( ' |' );
		}
		process.stdout.write( '\n' );
	}

	for( let row of rows ){
		if( row!=null ){
			for(let h of header){
				const a = h.column.split(/\./);
				let r = row;
				for( let i of a ) r=r[i];
				process.stdout.write( ' ' );
				process.stdout.write( printws( r, ( h.width || N ), h.type) );
				if(header[header.length-1].column!=h.column) process.stdout.write( ' |' );
			}
		}else{
			for(let h of header){
				process.stdout.write( '-' );
				process.stdout.write( '-'.repeat( h.width || N ) );
				if(header[header.length-1].column!=h.column) process.stdout.write( '-+' );
			}
		}
		process.stdout.write( '\n' );
	}

	if( (dots & 2)==2 ){
		for(let h of header){
			process.stdout.write( ' ' );
			process.stdout.write( printws( '...'.substr(0, h.title.length), ( h.width || N )) );
			if(header[header.length-1].column!=h.column) process.stdout.write( ' |' );
		}
		process.stdout.write( '\n' );
	}

	for(let h of header){
		process.stdout.write( '-' );
		process.stdout.write( '-'.repeat( h.width || N ) );
		if(header[header.length-1].column!=h.column) process.stdout.write( '-+' );
	}

	process.stdout.write( '\n' );

};
