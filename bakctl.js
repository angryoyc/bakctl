#!/usr/bin/node

'use strict';

const ESC          = '\x1b[';
const core         = new (require('./lib/core'))(__dirname + '/config.json');
const pkg          = require('./package');
const program      = require('./lib/commander_async');

const { print }            = require('./modules/print_methods');
const { printws }          = require('./modules/print_methods');
//const { print_table }      = require('./modules/print_methods');

//const printws      = require('./modules/printws');
//const table        = require('./modules/table');

const stringifyUrl = require ('./modules/stringifyUrl');

program
	.name(pkg.name)
	.version(pkg.version)
	.description( 'Утилита управления резервными копиями');

program
	.command('help')
	.alias('man')
	.long( '[<commandname>] [<commandname2>] [<commandname3>] [<commandname4>] [<commandname5>]' )
	.option( '-n', '--nocolor', 'Выводить только в одном цвете', null, 'false' )
	.option( '-f', '--full', 'Полный help по всем командам', null, 'false' )
	.option( '-m', '--md', 'Выводить используя разметку md', null, 'false' )
	.description('Вывод короткой справки по командам CLI')
	.action( async function(cmd){
		const color = !('-n' in cmd.options);
		const full = !!('-f' in cmd.options);
		const md = !!('-m' in cmd.options);
		const style = md?2:(color?1:0);
		if( cmd.values && Object.keys(cmd.values).length>0 ){
			const commandnames =  Object.keys(cmd.values).sort().map(c=>{ return cmd.values[c];}).filter(c => {return c;});
			await program.help_of_commands( commandnames, style );
		}else{
			await program.help_main( style );
			if( full ){
				await program.help_full( style );
			}
		}
	});

program
	.command('conf')
	.description('Конфигурация')
	.action( async function( cmd, argv ){
		argv.conf = await core.conf();
		if( cmd.commands && Object.keys(cmd.commands).length>0 ){
			await program.defaultAction(cmd, argv);
		}else{
			//console.log( JSON.stringify( argv.conf, null, 4 ) );
		}
	});

program
	.command('ls')
	.option( '-a', '--all', 'Выводить все сведения', null, 'false' )
	.option( '-c', '--commandstyle', 'Выводить в формате комманды, пригодной для исполнения утилитой BAKCTL', null, 'false' )
	.description('Задачи резервного копирования. Список.')
	.action( async function( cmd, argv ){
		const all = !!('-a' in cmd.options);
		const ascommand = !!('-c' in cmd.options);
		argv.conf = await core.conf();
		if( !argv.conf.objects ) argv.conf.objects = [];
		argv.position = 'all';
		let list = await core.task_details( argv );
		showTaskList( list, all, ascommand );
	});

program
	.command('add')
	.description('Добавить задачу')
	.long('<src> <dst>')
	.option( '-p', '--plan <plan>', 'План хранения архивных файлов (см. комманду plan)', null, 'false' )
	.option( '-g', '--group <group>', 'Присвоить группу <group>', null, 'false' )
	.option( '-i', '--id <id>', 'Присвоить ID=<id>', null, 'false' )
	.option( '-c', '--checksum <yesno>', 'Установить/сбросить проверку контрольной суммы', null, 'false' )
	.option( '-d', '--delafter <yesno>', 'Если "yes", то удалять источник после удачного исполнения задания (пока не работает)', null, 'false' )
	.action( async function( cmd, argv ){
		if( 'src' in cmd.values ) argv.src = cmd.values['src'];
		if( 'dst' in cmd.values ) argv.dst = cmd.values['dst'];
		if( ('-p' in cmd.options) &&  cmd.options['-p'].values['plan']){
			argv['plan'] = cmd.options['-p'].values['plan'];
		}else{
			argv.plan = '24,7,4,12';
		}
		if( ('-g' in cmd.options) &&  cmd.options['-g'].values['group']){
			argv['group'] = cmd.options['-g'].values['group'];
		}
		if( ('-i' in cmd.options) &&  cmd.options['-i'].values['id']){
			argv['id'] = cmd.options['-i'].values['id'];
		}
		if( ('-c' in cmd.options) &&  cmd.options['-c'].values['yesno']){
			argv['checkmd5'] = cmd.options['-c'].values['yesno'];
		}
		if( ('-d' in cmd.options) &&  cmd.options['-d'].values['yesno']){
			argv['delafter'] = cmd.options['-d'].values['yesno'];
		}
		await core.conf_add( argv );
	});

program
	.command('plan')
	.description('Задать режим хранения резервных копий')
	.long('<position> <plan>')
	.note('Устанавливаеется режим для выбранных позиций. PLAN - строка в формате /\\d+-\\d+-\\d+-\\d+/ - то есть  четыре  числа разделённые знаком "-".\n\
Каждое из чисел определяет количество резервных копий, которое будет оставлено для каждого из видов архивов: ежечасных, ежемесячных, еженедельных\n\
и ежемесячных соответственно. Например, строка "24-7-4-12" задаст режим хранения при котором будут сохраняться резервные копии за  каждый  час  в\n\
течении последних суток (24 ежечасных копий), за каждый день в течении последней недели (7 ежедневных копий), на каждую неделю в  течении  месяца\n\
(4 еженедельных копий)) и за каждый месяц года (12 ежемесячных).')
	.action( async function( cmd, argv ){
		if( 'position' in cmd.values ) argv.position = cmd.values['position'];
		if( 'plan' in cmd.values ){
			argv.plan = cmd.values['plan'];
		}else{
			argv.plan = '24,7,4,12';
		}
		await core.task_set_val( argv );
	});

program
	.command('del')
	.description('Удалить задачу')
	.long('<position>')
	.note('Значение position может принимать значения отображаемые по команде ls. Так же вместо позиции можно указать ключевое слово "all" для полной очистки списка задач')
	.action( async function( cmd, argv ){
		if( 'position' in cmd.values ) argv.position = cmd.values['position'];
		await core.conf_del( argv );
	});

program
	.command('checkmd5')
	.alias('checksum')
	.description('Задать проверку котрольной суммы')
	.long('<position> <onoff>')
	.note('Отдельным пунктам можно назначать проверку контрольной суммы')
	.action( async function( cmd, argv ){
		if( 'position' in cmd.values ) argv.position = cmd.values['position'];
		if( 'onoff' in cmd.values ){
			argv.onoff = (
				( cmd.values['onoff']==1 ) ||
				( cmd.values['onoff'].toLowerCase()=='yes' ) ||
				( cmd.values['onoff'].toLowerCase()=='true' ) ||
				( cmd.values['onoff'].toLowerCase()=='да' )
			);
		}else{
			throw new Error('Необходимо указать устанавливаемое значение: yes|no');
		}
		await core.task_set_checkmd5( argv );
	});

program
	.command('group')
	.description('Задать группу')
	.long('<position> <groupname>')
	.note('Отдельным пунктам можно назначать группы')
	.action( async function( cmd, argv ){
		if( 'position' in cmd.values ) argv.position = cmd.values['position'];
		if( 'groupname' in cmd.values ) argv.groupname = cmd.values['groupname'];
		await core.conf_group( argv );
	});

program
	.command('test')
	.description('Тестировать задание')
	.long('<position> <groupname>')
	.note('Тестирование источника и и приёмника в задании')
	.option( '-f', '--force', 'Создавать несуществующие директории (локально)', null, 'false' )
	.action( async function( cmd, argv ){
		if( 'position' in cmd.values ) argv.position = cmd.values['position'];
		argv.force = !!('-f' in cmd.options);
		await core.conf_test( argv );
	});

/*                                     OBJ                                                     */
program
	.command('task')
	.description('Параметры хранения резервных копий')
	.long('<position>')
	.action( async function( cmd, argv ){
		if( 'position' in cmd.values ) argv.position = cmd.values['position'];
		if( cmd.commands && Object.keys(cmd.commands).length>0 ){
			await program.defaultAction(cmd, argv);
		}else{
			//let list = await core.task_details( argv );
			//showTaskList( list, true );
			await showTask(argv);
		}
	});

program
	.command('task')
	.command('plan')
	.description('Установить параметры')
	.long('<crit> <val>')
	.note('Параметр VAL - количество сохраняемых резервных копий.\n\
CRIT - одно из четырёх ключевых слов: hourly, daily, weekly, monthly для ежечасных, ежедневных, еженедельных и\n\
ежемесячных архивов соответственно. Таким образом, команда задаёт сколько, например, ежедневных ахривов  будут\n\
хранится')
	.action( async function( cmd, argv ){
		if( 'crit' in cmd.values ) argv.crit = cmd.values['crit'];
		if( 'val' in cmd.values ) argv.val = cmd.values['val'];
		await core.task_set_val( argv );
	});

program
	.command('task')
	.command('show')
	.description('Показать все параметры задачи')
	.action( async function( cmd, argv ){
		await showTask(argv);
	});

async function showTask(argv, aslist){
	let list = await core.task_details( argv );
	if(list.length>0){
		if(list.length<2){
			const task = list[0];
			print( 'ID:\t\t' + task.id );
			print( 'Группа:\t\t' + (task.group || '-') );
			print( 'Источник:\t' + stringifyUrl(task.src) );
			print( 'Получатель:\t' + stringifyUrl(task.dst) );
			print( 'План:\t\t' + task.plan.monthly + '-' + task.plan.weekly + '-' + task.plan.daily + '-' + task.plan.hourly);
			print( 'Статус:\t\t' + (task.laststate || '-') );
			print( 'Сообщение:\t' + (task.message || '-') );
			print( 'md5:\t\t' + (task.checkmd5?'check':'no check') );
		}else{
			if(aslist){
				showTaskList( list, true );
			}else{
				//throw new Error( 'Несколько задач подходящих под указанный идентификатор: "' + argv.position + '". Укажите задачу по ID или по порядковому номеру' );
			}
		}
	}else{
		throw new Error( 'Задача не найдена: ' + argv.position );
	}
}

program
	.command('task')
	.command('ls')
	.description('Список существующих архивов')
	.action( async function( cmd, argv ){
		let result =  await core.task_ls( argv );
		for( let f of result ){
			process.stdout.write( f.basename + '\t' + f.size + '\n');
		}
	});

program
	.command('stat')
	.description('Общая статистика')
	.long('<position>')
	.action( async function( cmd, argv ){
		if( 'position' in cmd.values ){
			argv.position = cmd.values['position'];
		}else{
			argv.position = 'all';
		}
		await core.task_stat( argv );
	});

program
	.command('task')
	.command('clear')
	.description('Очистить ресурсы хранения от просроченных старых резервных копий')
	.action( async function( cmd, argv ){
		if( 'position' in cmd.values ) argv.position = cmd.values['position'];
		await core.clear( argv );
	});

program
	.command('task')
	.command('start')
	.option( '-n', '--nocolor', 'Выводить только в одном цвете', null, 'false' )
	.description('Начать операцию резервного копирования')
	.action( async function( cmd, argv ){
		argv.color = !('-n' in cmd.options);
		let errors = await core.start( argv );
		await error_response( argv, errors );
	});
/*                                            END                                              */

program
	.command('start')
	.option( '-n', '--nocolor', 'Выводить только в одном цвете', null, 'false' )
	.long('<position>')
	.description('Начать операцию резервного копирования')
	.action( async function( cmd, argv ){
		if( 'position' in cmd.values ) argv.position = cmd.values['position'];
		argv.color = !('-n' in cmd.options);
		let errors = await core.start( argv );
		await error_response( argv, errors );
	});

program
	.command('clear')
	.long('<position>')
	.description('Очистить ресурсы хранения от просроченных старых резервных копий')
	.action( async function( cmd, argv ){
		if( 'position' in cmd.values ) argv.position = cmd.values['position'];
		await core.clear( argv );
	});

program
	.command('exit')
	.alias('quit')
	.alias('выход')
	.description('Выход из интерфейса командной строки')
	.action(async function() {
		program.stop();
		process.stdout.write('Bye Bye...\n');
		process.exit(0);
	});

program
	.command('do')
	.long('<commandfilename>')
	.description('Выполненить командный файл')
	.note('Команды из файла выполняются одна за одной. Строки, первый значимый (не пробельный) символ которых является знак "#" (решётка) пропускаются.\n\nТаким образом для коментирования текста командного файла можно использовать этот символ (решётка).')
	.action(async function(cmd) {
		const commandfilename = cmd.values['commandfilename'];
		await program.doit( commandfilename, async function( line ){
			await program.parse( program.splitCommand( line ) );
		});
	});


//                           =========== START =============
program.start(
	async () =>{
		//onstart
	},
	async () =>{
		//onstop
		await core.stop();
	},
	async (err) =>{
		//onerror
		process.stdout.write( ESC + '31m' +  'ERR: ' + ESC + '0m' +  err.message + '\n');
		//await acc_lib.stop();
	},
	async () =>{
		//title
		process.stdout.write( '\n' + 'Версия: ' + program.getVersion()  + '\n');
		process.stdout.write( '\n' + 'Используй команду help, если не знаешь, что делать дальше \n\n' );
	}
);
//                           -------------------------------


// ----------------------------- Вспомогательные функции ---------------------

function showTaskList( list, full, ascommand ){
	if(!ascommand){
		if( list.length>0 ){
			let max = 0;
			for( let i in list ){
				let obj = list[ i ];
				max = Math.max( max, ('"'+stringifyUrl( obj.src ) + '" "' + stringifyUrl( obj.dst )+'"').length);
			}
			for( let i in list ){
				let obj = list[ i ];
				process.stdout.write( printws((1*i+1) + ')', 4) + ' | '	+ (obj.checkmd5?ESC + '31m' + obj.id + ESC + '0m':obj.id) 
				+ ' | ' + printws((obj.src.proto + '->' + (obj.dst.proto || 'file')), 11) + ' | ' + printws(obj.delafter?'X':' ', 1) + ' | ' + printws((obj.group || ''), 7) + ' | ' + printws( '"' + stringifyUrl( obj.src ) + '" "' + stringifyUrl( obj.dst ) +'"', max) );
				if(full){
					if(!obj.plan) obj.plan = {};
					process.stdout.write(' | ' + printws(obj.plan.hourly || 0, 2) + ' | ' + printws(obj.plan.daily || 0,2) + ' | ' + printws(obj.plan.weekly || 0, 2) + ' | ' + printws(obj.plan.monthly || 0,2) + ' ');
				}
				process.stdout.write('\n');
			}
		}else{
			process.stdout.write( 'нет задач резервного копирования\n' );
		}
	}else{
		process.stdout.write('del all\n');
		for( let i in list ){
			let obj = list[ i ];
			process.stdout.write('add "' + stringifyUrl( obj.src, /*showpassword*/true ) + '" "' + stringifyUrl( obj.dst, /*showpassword*/true ) + '"');
			obj.plan = obj.plan || {};
			process.stdout.write(' -p "' +[ (obj.plan.hourly || 0), ( obj.plan.daily || 0 ) , ( obj.plan.weekly || 0 ), ( obj.plan.monthly || 0 ) ].join('-') + '"');
			if( obj.group ) process.stdout.write(' -g "' + obj.group + '"');
			process.stdout.write(' -i "' + obj.id + '"');
			process.stdout.write(' -c "' + (obj.checkmd5?'yes':'no') + '"');
			process.stdout.write(' -d "' + (obj.delafter?'yes':'no') + '"');
			process.stdout.write('\n');
		}
	}
}

async function error_response(argv, errors){
	if( errors.length>0 ){
		await core.notification_send(
			{
				color: argv.color,
				text: 'Ошибки резервного копирования:\n' + errors.join('\n')
			}
		);
	}
}
