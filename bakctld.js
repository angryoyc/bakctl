#!/usr/bin/node

'use strict';

const { print }           = require('./modules/print_methods');
const { print_object }    = require('./modules/print_methods');
const controllers         = require('./modules/controllers');
const conf                = require("./config.json");
const core                = require("./lib/core");
const http                = require('http');
const url                 = require('url');
const { URLSearchParams } = require('url');

const defa_port = 9172;
const defa_host = '0.0.0.0'
const ESC = '\x1b[';

const worker = async (req, res)=>{
	var r=url.parse(req.url);
	const p = new URLSearchParams(r.query);
	const params={};
	for (const [name, value] of p) { params[name]=value; };
	const path = r.pathname.split(/\//); //
	if(path[1] in controllers){
		try{
			let body = await getReqBody(req);
			for (const name in body) { params[name]=body[name]; };
			if( conf.server && conf.server.verbose>0){
				print( req.method + ' Requested /' + path[1]);
				if( conf.server.verbose>1){
					print_object(params);
				}
			}
			await controllers[path[1]](req, res, params);
		}catch(e){
			let mess = (e || {message:''}).message;
			print( ESC + '31m' + 'ERR: ' + ESC + '0m' + mess);
			res.statusCode = 500;
			res.statusMessage = 'Internal Server Error';
			res.setHeader('Content-Type', 'text/plain; charset=utf-8');
			res.end('ERR: ' + mess, 'UTF8');
		}
	}else{
		if(path[1]==''){
			res.end('');
		}else{
			res.statusCode = 404;
			res.statusMessage = 'Not found';
			res.end('Not found [' + path[1] + ']');
		}
	};
};

async function start(){
	try{
		const server = http.createServer(worker);
		server.timeout = 480000;
		let port = defa_port;
		let host = defa_host;
		if(conf && conf.server){
			port = conf.server.port || defa_port;
			host = conf.server.host || defa_host;
		}
		server.on('error', (e) => {
			if (e.code === 'EADDRINUSE') {
				print(ESC + '31m' + 'ERR: ' + ESC + '0m' + 'Address in use, retrying...');
				setTimeout(() => {
					server.close();
					server.listen(port, host);
				}, 1000);
			}
		});
		server.listen(port, host, function(err){
			if(!err) print('HTTP server listen on ' +  ESC + '32m' + host + ':' + port + ESC + '0m');
		});
	}catch(e){
		print( ESC + '31m' + 'ERR: ' + ESC + '0m' + e.mess );
	};
};

start();

async function getReqBody(req){
	return new Promise(function(resolve, reject){
		if (req.method === 'POST') {
			let body = '';
			req.on('data', chunk => {
				body += chunk.toString(); // convert Buffer to string
			});
			req.on('end', () => {
				resolve(eval('(' + body + ')'));
			});
			req.on('error', (err) => {
				reject(err);
			});
		}else{
			return {};
		}
	});
}
