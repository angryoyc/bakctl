'use strict';

const require_tree = require('require-tree');

//let pdb;

module.exports = function(config_path, aslib){
	const config_path_real=(config_path || '../config.json');
	const config = require(config_path_real);
	if( config.confpath ){
		let methods = require_tree('../methods');
		for(let m in methods){
			this[m] = (methods[m])(config, aslib || false);
		};
	}else{
		throw new Error("Путь основного конфигурационного файла не обнаружен в файле " + config_path_real);
	}
};

