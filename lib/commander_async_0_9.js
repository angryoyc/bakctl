'use strict';
'version 0.9';

let version;
let description;
let name;

let commands = {};
let options = {};


const printws = require('../modules/printws');

const { clearcommands } = require('../modules/utils');

exports.name = function (n) { name = n; return exports; };
exports.getName = function () { return name || ''; };

exports.version = function (v) { version = v; return exports; };
exports.getVersion = function () { return version || ''; };

exports.description = function (d) { description = d; return exports; };
exports.getDescription = function () { return description || ''; };

let lastcommand = null;
let lastoption = null;

const template = {};

const readline = require('readline');
let rl;
const ESC = '\x1b[';

let onstop = null;
let onstart = null;
let onerror = null;
let ontitle = null;

exports.get_template = function (commandnames) {
	let commands = template.commands;
	let cmd;
	for (let commandname of commandnames) {
		cmd = get_template(commandname, commands);
		if (cmd) {
			commands = cmd.commands;
		} else {
			break;
		}
	}
	return cmd;
};

function get_template(commandname, commands) {
	if (commandname in commands) {
		const cmd = commands[commandname];
		return cmd;
	} else {
		return null;
	}
}

exports.list = function (idata, parent, level) {
	const data = idata || [];
	const prnt = parent || template;
	const lvl = level || 0;
	const index = {};
	for (let k of Object.keys(prnt.commands).sort()) {
		let cmd = prnt.commands[k];
		if (!(cmd.name in index)) {
			index[cmd.name] = true;
			data.push({ level: lvl, cmd: cmd });
			if (cmd.commands) {
				exports.list(data, cmd, lvl + 1);
			}
		}
	}
	return data;
};

exports.command = function (cmdname) {
	if (template && template.commands && (cmdname in template.commands)) {
		return template.commands[cmdname].self;
	} else {
		return new cmd(cmdname, template);
	}
};

function cmd(cmdname, templ) {
	let self = this;
	if (!templ.commands) templ.commands = {};
	const myconf = templ.commands[cmdname] = {};

	myconf.name = cmdname;
	myconf.options = {};
	myconf.commands = {};
	myconf.self = self;

	self.description = function (d) {
		myconf.description = d;
		return self;
	};

	self.note = function (n) {
		myconf.note = n;
		return self;
	};

	self.alias = function (als) {
		templ.commands[als] = templ.commands[myconf.name];
		return self;
	};

	self.long = function (long) {
		myconf.long = long;
		return self;
	};

	self.option = function (optname, long, descr) {
		const a = long.split(/ +/);
		myconf.options[optname] = {
			name: optname,
			long: long,
			description: descr
		};
		myconf.options[a[0]] = myconf.options[optname];
		return self;
	};

	self.action = function (_f) {
		if (!_f || !(_f instanceof Function)) throw new TypeError('!_f of _f not Function');
		const AsyncFunction = (async () => { }).constructor;
		if (_f instanceof AsyncFunction) {
			// async
			myconf.asyncAction = true;
		} else {
			myconf.asyncAction = false;
		}
		myconf.actionFunction = _f;
		return self;
	};

	self.command = function (cmdname) {
		if (cmdname in myconf.commands) {
			return myconf.commands[cmdname].self;
		} else {
			return new cmd(cmdname, myconf);
		}
	};
	return self;
}

async function defaultAction(maincmd, iarg) {
	const argv = iarg || {};
	for (let cmdname in maincmd.commands) {
		const cmd = maincmd.commands[cmdname];
		if (cmd.actionFunction) {
			if (cmd.asyncAction) {
				await cmd.actionFunction(cmd, argv);
				return;
			} else {
				cmd.actionFunction(cmd, argv);
				return;
			}
		} else {
			await defaultAction(cmd, argv);
		}
	}
}

exports.defaultAction = defaultAction;

exports.parse = async function (argv) {
	lastoption = null;
	lastcommand = null;
	commands = {};
	options = {};
	for (let i = 0; i < argv.length; i++) {
		const item = argv[i];
		if (lastoption) {
			// очередной параметр
			let a = ((lastoption.template.long || '').match(/<(.+?)>/g) || []).map(p => { return p.replace(/^</, '').replace(/>$/, ''); });
			if (lastoption.values.length < a.length) {
				// очередной параметр опции
				const parname = a[lastoption.values.length];
				lastoption.values.push({ parname: parname, value: item });
			} else {
				f1(item);
			}
		} else if (lastcommand) {
			let a = ((lastcommand.template.long || '').match(/<(.+?)>/g) || []).map(p => { return p.replace(/^</, '').replace(/>$/, ''); });
			if (lastcommand.values.length < a.length) {
				if (item.match(/^-/)) {
					f1(item);
				} else {
					// очередной параметр комманды
					const parname = a[lastcommand.values.length];
					lastcommand.values.push({ parname: parname, value: item });
				}
			} else {
				f1(item);
			}
		} else {
			f1(item);
		}
	}
	await validate();
};

function f1(item) {
	if (item.match(/^-/)) {
		// новая опция команды
		if (item in (lastcommand ? lastcommand.template : template).options) {
			const optname = (lastcommand ? lastcommand.template : template).options[item].name;
			lastoption = (lastcommand ? lastcommand.options : options)[optname] = { template: (lastcommand ? lastcommand.template : template).options[optname], values: [] };
		} else {
			if (lastcommand) {
				throw new Error('Неизвестная опция команды ' + lastcommand.name + ' :: опция - ' + item);
			} else {
				throw new Error('Неизвестная опция  - ' + item);
			}
		}
	} else {
		// новая команда
		if (item in ((lastcommand ? lastcommand.template : template).commands || {})) {
			let cmnds;
			if (lastcommand) {
				lastcommand.commands = cmnds = lastcommand.commands || {};
			} else {
				cmnds = commands;
			}
			lastcommand = { name: item, template: (lastcommand ? lastcommand.template : template).commands[item], options: {}, values: [] };
			cmnds[lastcommand.name] = lastcommand;
			lastoption = null;
		} else {
			//console.log(item, ((lastcommand?lastcommand.template:template).commands) );
			throw new Error('Неизвестная команда: ' + item);
		}
	}
}

async function validate() {
	for (let cmdname in commands) {
		const cmd = commands[cmdname];
		validate_command(cmd);
		if (cmd.actionFunction) {
			if (cmd.asyncAction) {
				//await cmd.template.actionFunction( cmd );
				await cmd.actionFunction(cmd, {});
			} else {
				//cmd.template.actionFunction( cmd );
				cmd.actionFunction(cmd, {});
			}
		} else {
			await defaultAction(cmd, {});
		}
	}
}

function validate_command(cmd, parent) {
	const values = {};
	for (let val of cmd.values) values[val.parname] = val.value;
	cmd.values = values;
	if (cmd.options) {
		for (let optname in cmd.options) {
			const opt = cmd.options[optname];
			const optval = {};
			for (let val of opt.values) optval[val.parname] = val.value;
			opt.values = optval;
		}
	}
	if (cmd.commands) {
		for (let cmdname in cmd.commands) validate_command(cmd.commands[cmdname], cmd);
	}
	if (cmd.template.actionFunction) cmd.actionFunction = cmd.template.actionFunction;
	if (cmd.template.asyncAction) cmd.asyncAction = cmd.template.asyncAction;
	delete cmd.template;
	if (parent) cmd.parent = parent;
}

exports.splitCommand = function (str) {
	let frases = str.match(/["'](.+?)["']/g) || []; //'
	frases = frases.map(w => {
		return w.replace(/^["']/, '').replace(/["']$/, ''); //'
	});
	let str_r = str.replace(/(["'].+?["'])/g, '|||'); //'
	const a = str_r.split(/ +/);
	const aa = a.map(p => {
		if (p == '|||') {
			return frases.shift();
		} else {
			return p;
		}
	});
	return aa;
};

exports.start = async function (fStart, fStop, fError, fTitle) {
	onstart = fStart;
	onstop = fStop;
	onerror = fError;
	ontitle = fTitle;
	// ONSTART
	try {
		if (typeof (onstart) == 'function') await onstart();
	} catch (err) {
		if (typeof (onerror) == 'function') {
			try {
				await onerror(err);
				exports.stop();
			} catch (err) {
				process.stdout.write('ERROR: ' + err.message + '\n');
			}
		} else {
			process.stdout.write('ERROR: ' + err.message + '\n');
		}
	}

	if (process.argv.length > 2) {
		try {
			await exports.parse(process.argv.slice(2));
			exports.stop();
		} catch (err) {
			if (typeof (onerror) == 'function') {
				try {
					await onerror(err);
					exports.stop();
				} catch (err) {
					process.stdout.write('ERROR: ' + err.message + '\n');
				}
			} else {
				process.stdout.write('ERROR: ' + err.message + '\n');
			}
		}
	} else {
		const inputdata = await awaitData(100);
		if (inputdata) {
			try {
				for ( let line of clearcommands(inputdata) ) {
					
					await exports.parse( exports.splitCommand( line ) );
				}
			} catch (err) {
				if (typeof (onerror) == 'function') {
					try {
						await onerror(err);
						exports.stop();
					} catch (err) {
						process.stdout.write('ERROR: ' + err.message + '\n');
					}
				} else {
					process.stdout.write('ERROR: ' + err.message + '\n');
				}
			}
			exports.stop();
		} else {
			try {
				if (typeof (ontitle) == 'function') await ontitle();
			} catch (err) {
				if (typeof (onerror) == 'function') {
					try {
						await onerror(err);
						//exports.stop();
					} catch (err) {
						process.stdout.write('ERROR: ' + err.message + '\n');
					}
				} else {
					process.stdout.write('ERROR: ' + err.message + '\n');
				}
			}
			rl = readline.createInterface({
				input: process.stdin,
				output: process.stdout
			});
			rl.on('SIGINT', () => {
				process.stdout.write((ESC + '0m') + '\nBye Bye...\n');
				exports.stop();
			});
			await loop(onerror);
		}
	}
};

exports.stop = async function () {
	if (rl) rl.close();
	rl = null;
	if (typeof (onstop) == 'function') await onstop();
	process.exit(0);
};

async function loop(onerror) {
	rl.question((ESC + '93m') + exports.getName() + '> ' + (ESC + '0m') + (ESC + '96m'), async (answer) => {
		try {
			if (answer) {
				const a = exports.splitCommand(answer);
				process.stdout.write(ESC + '0m');
				await exports.parse(a);
				if (rl) {
					await loop(onerror);
				} else {
					return;
				}
			} else {
				await loop(onerror);
			}
		} catch (err) {
			if (typeof (onerror) == 'function') {
				try {
					await onerror(err);
				} catch (err) {
					process.stdout.write('ERROR: ' + err.message + '\n');
				}
			} else {
				process.stdout.write('ERROR: ' + err.message + '\n');
			}
			await loop(onerror);
		}
	});
}

exports.help_of_commands = async function (commandnames, style) {
	const ESC36B = ['', ESC + '36m', '*'][style || 0] || ''; const ESC36E = ['', ESC + '0m', '*'][style || 0] || '';

	const cmdtemplate = exports.get_template(commandnames);
	const a = [];
	const commandnames_temp = [];

	for (let c of commandnames) {
		a.push(c);
		commandnames_temp.push(exports.get_template(a));
	}

	if (cmdtemplate) {
		//if(cmdtemplate.description) process.stdout.write( '\n' + ESC36B + commandnames.join(' ') + ESC36E + ' - ' + cmdtemplate.description || '\n' );
		if (cmdtemplate.description) process.stdout.write('\n' + (cmdtemplate.description || '\n'));
		process.stdout.write('\n\nСинтаксис:\n\n');
		//+ ESC93B + exports.getName() + '> ' + ESC93E
		process.stdout.write('\t' + ((style == 2) ? '' : ESC36B) +
			commandnames_temp
				.map((c, i) => {
					return (c.name + (c.long ? (' ' + c.long) : '') + ((c.options && (Object.keys(c.options).length > 0) && ((commandnames_temp.length - 1) == i)) ? ' [options]' : '') + ((c.commands && (Object.keys(c.commands).length > 0) && ((commandnames_temp.length - 1) == i)) ? ' [subcommands]' : ''));
				})
				.join(' ') +
			' ' + ((style == 2) ? '' : ESC36E) + '\n');
		if (cmdtemplate.note) process.stdout.write(('\n' + cmdtemplate.note + '\n') || '\n');
		if (cmdtemplate.commands && (Object.keys(cmdtemplate.commands).length > 0)) {
			process.stdout.write('\nПодкоманды:\n\n');
			for (let scmd of Object.keys(cmdtemplate.commands)) {
				const scmdtemplate = exports.get_template(commandnames.concat([scmd]));
				process.stdout.write('\t' + ((style == 2) ? '' : ESC36B) + printws(scmd, 12) + ((style == 2) ? '' : ESC36E) + '\t- ' + scmdtemplate.description + '\n');
			}
		}
		if (cmdtemplate.options && (Object.keys(cmdtemplate.options).length > 0)) {
			process.stdout.write('\nОпции: \n\n');
			for (let optname of Object.keys(cmdtemplate.options).sort().filter(c => { return c.match(/^--/); })) {
				const opt = cmdtemplate.options[optname];
				process.stdout.write('\t' + opt.name + ' | ' + printws(opt.long, 20) + ' - ' + opt.description + '\n');
			}
		}
		process.stdout.write('\n');
	} else {
		process.stdout.write('Неизвестная команда: ' + commandnames.join(' ') + '\n');
	}
};

exports.help_main = async function (style) {
	const ESC32B = ['', ESC + '32m', '**'][style || 0] || ''; const ESC32E = ['', ESC + '0m', '**'][style || 0] || '';
	const ESC36B = ['', ESC + '36m', '*'][style || 0] || ''; const ESC36E = ['', ESC + '0m', '*'][style || 0] || '';
	if (style == 2) {
		process.stdout.write('\n# ' + exports.getName().toUpperCase() + '\n\n');
	} else {
		process.stdout.write('\n' + exports.getName().toUpperCase() + '\n\n');
	}
	process.stdout.write('версия: ' + exports.getVersion() + '\n');
	process.stdout.write('\n' + exports.getDescription() + '\n');
	process.stdout.write('\nИспользование:\n\n\t' + exports.getName() + ' <cmd> [<cmdoption>[ <cmdoptionarg>]] [<subcommand> [<subcoption>[ <subcoptionarg>]]] ...\n\n');
	process.stdout.write('Доступны следующие команды:\n' + '\n');
	const list = exports.list();
	if (style == 2) {
		let max = 0;
		for (let row of list) {
			max = Math.max(max, ('  '.repeat(row.level + 3) + ((row.level != 0) ? '  ' : '✓') + ' ' + row.cmd.name).length);
		}
		for (let row of list) {
			process.stdout.write(printws('  '.repeat(row.level + 3) + ((row.level != 0) ? '  ' : '✓') + ' ' + row.cmd.name, max + 2) + row.cmd.description + '\n');
		}
		process.stdout.write('\nИспользуйте: \'' + exports.getName() + ' help \\<cmd\\>\' для получения помощи по конкретной команде\n\n');
	} else {
		for (let row of list) {
			process.stdout.write('  '.repeat(row.level + 3) + ((row.level != 0) ? '  ' : (ESC32B + '✓' + ESC32E)) + ' ' + ESC36B + row.cmd.name + ESC36E + '\r\t\t\t\t' + row.cmd.description + '\n');
		}
		process.stdout.write('\nИспользуйте: \'' + exports.getName() + ' help <cmd>\' для получения помощи по конкретной команде\n\n');
	}
};

exports.help_full = async function (style) {
	const ESC96B = ['', ESC + '96m', '*'][style || 0] || ''; const ESC96E = ['', ESC + '0m', '*'][style || 0] || '';
	const list = exports.list();
	let aCmd = [];
	for (let row of list) {
		aCmd[row.level] = row.cmd.name;
		aCmd = aCmd.slice(0, row.level + 1);
		let cmd = aCmd.map(c => { return c; }).join(' ');
		if (style == 2) {
			//process.stdout.write( '\n\n--' + '-'.repeat( cmd.length ) + '\n' );
			process.stdout.write('\n\n\n\n## ' + cmd + '\n');
		} else {
			process.stdout.write('\n\n' + ESC96B + cmd + ESC96E + '\n');
			process.stdout.write('-'.repeat(cmd.length) + '\n');
		}
		await exports.help_of_commands(aCmd, style);
	}
};

// Ожидание данных с STDIN. Ожидание прерывается, если данные не поступали дольше чем N миллисекунд ()
async function awaitData(N) {
	return new Promise((resolve, reject) => {
		let inputdata = '';
		let chunk = '';
		let id;
		let to = () => {
			if (chunk) {
				inputdata = inputdata + chunk;
				chunk = '';
				id = setTimeout(to, N * 3);
			} else {
				resolve(inputdata + chunk);
			}
		};
		id = setTimeout(to, N);
		const on_data = (data) => {
			chunk = chunk + data.toString('utf8');
		}
		const on_err = (err) => {
			clearTimeout(id);
			unbind();
			reject(err);
		}
		const on_end = () => {
			clearTimeout(id);
			unbind();
			resolve(inputdata + chunk);
		}
		const unbind = ()=>{
			process.stdin.removeListener('data', on_data);
			process.stdin.removeListener('error', on_err);
			process.stdin.removeListener('end', on_end);
		}

		process.stdin.on('data', on_data);
		process.stdin.on('error', on_err);
		process.stdin.on('end', on_end);

	});
}
