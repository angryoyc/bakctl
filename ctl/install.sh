#!/bin/bash

PROGNAME='bakctl'
DIRNAME=`pwd`
USR=${USER}

source ./ctl/uninstall.sh

sudo ln -s ${DIRNAME}/${PROGNAME}.js /usr/bin/${PROGNAME}
sudo mkdir -p /var/lib/${PROGNAME}
sudo chmod 755 /var/lib/${PROGNAME}
sudo chown ${USR}:${USR} /var/lib/${PROGNAME}
if [ -f "/var/lib/${PROGNAME}/conf.json" ]; then
	echo "Файл конфигурации уже существует. Оставлен без изменения."
else
	echo "{}" > "/var/lib/${PROGNAME}/conf.json"
	echo "Новый файл конфигурации создан"
fi


if [ -f "/usr/bin/${PROGNAME}" ]; then
	echo "Приложение BAKCTL установленно. Устанавливаются зависимости..."
else
	echo "Что-то не получилось..."
fi

