'use strict';
/*
*  отправка оповещения админу об ошибках резервного копирования  [-]
*/

const ESC     = '\x1b[';
const fs = require('fs');
const { promisify } = require('util');
const readdir =  promisify(fs.readdir);
const exec = require('../modules/exec');
const path = require('path');
const isArray = function(a){return (!!a) && (a.constructor === Array);};

module.exports = function ( config ) {
	return async function (iarg) {

		const arg = iarg || {color:true, text:'-- нет данных, но что-то явно не так --'};

		try{
			let toolspath = path.dirname(__dirname) + '/tools';
			let tools_dir = await readdir(path.dirname(__dirname) + '/tools');
			let tools_index = {};
			tools_dir.forEach( tool =>{
				tools_index[tool] =  toolspath + '/' + tool;
			});

			process.stdout.write('\n    Отправка оповещения об ошибках администраторам\n');

			if( isArray( config.admins ) && (config.admins.length>0) ){
				for( let admin of config.admins ){
					if( isArray( admin.contacts ) ){
						for( let contact of admin.contacts ){
							let transport = Object.keys( contact )[0];
							let address = contact[transport];
							if( transport in tools_index ){
								try{
									if(arg.color){
										process.stdout.write( ' '.repeat(7) + ESC + '32m' + '✔' + ESC + '0m' + ESC + '2m' + ' ' + transport + ' --> ' + address + ' ... ' + ESC + '0m' );
									}else{
										process.stdout.write( ' '.repeat(7) + '✔' + ' ' + transport + ' --> ' + address + ' ... ' );
									}

									await exec( tools_index[ transport ], [ address ], arg.text );

									if(arg.color){
										process.stdout.write('[ ' + ESC + '32m' + 'ok' + ESC + '0m' + ' ]\n');
									}else{
										process.stdout.write('[ ok ]\n');
									}
									process.stdout.write('\n');
								}catch(e){
									process.stdout.write(e.message  + '\n');
								}
							}
						}
					}
				}
			};
		}catch(e){
			throw e;
		}
		return;
	};
};

