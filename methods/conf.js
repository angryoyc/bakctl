'use strict';
/*
*  импорт данных [-]
*/

const fs = require('fs');
const { promisify } = require('util');
const writeFile = promisify(fs.writeFile);
const readFile = promisify(fs.readFile);
const stat = promisify(fs.stat);

module.exports = function ( config ) {
	return async function (iarg, idata) {
		const arg = iarg || {};
		const data = idata || {};
		let conf;
		if( config.confpath ){
			try{
				let stats = stat( config.confpath );
				conf = require( config.confpath );
			}catch(err){
				conf = {};
			}
		}else{
			conf = {};
		}
		await writeFile( config.confpath, JSON.stringify(conf, null, 4), 'utf8');
		return conf;
	};
};
