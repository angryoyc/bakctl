'use strict';
/*
*  добавить объект [-]
*/

const fs = require('fs');
const { promisify } = require('util');
const writeFile = promisify(fs.writeFile);
const values = { 'hourly':true, 'daily':true, 'weekly':true, 'monthly':true };
const getPositions = require ('../modules/getPositions');

module.exports = function ( config ) {
	return async function (iarg, idata) {
		const arg = iarg || {};
		const data = idata || {};
		let conf = require( config.confpath );
		if( ('position' in arg) && ('onoff' in arg) ){
			let list = getPositions( conf, arg.position );
			for( let obj of list ){
				obj.checkmd5 = !!arg.onoff;
			}
			process.stdout.write('Установлено для ' + list.length + ' элементов \n');
			await writeFile( config.confpath, JSON.stringify(conf, null, 4), 'utf8');
			process.stdout.write('сохранено\n');
		}else{
			throw new Error('Необходимо указать позицию или ключевое слово all');
		}
		return;
	};
};
