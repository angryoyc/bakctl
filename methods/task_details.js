'use strict';
/*
*  добавить объект [-]
*/

//const fs = require('fs');
//const { promisify } = require('util');
//const writeFile = promisify(fs.writeFile);
const getPositions = require ('../modules/getPositions');

module.exports = function ( config ) {
	return async function (iarg, idata) {
		const arg = iarg || {};
		const data = idata || {};
		let conf = require( config.confpath );
		if( 'position' in arg ){
			let list = getPositions( conf, arg.position );
			return list;
		}else{
			throw new Error('Необходимо указать конкретную позицию или ключевое слово all для всех объектов');
		}
		return;
	};
};
