'use strict';

/*
* Список архивов [-]
*
*/

const fs = require('fs');
const { promisify } = require('util');
const stat = promisify(fs.stat);

const path = require('path');

const exec = require('../modules/exec');

module.exports = function ( config ) {
	return async function (iarg, idata) {
		const arg = iarg || {};
		const data = idata || {};
		let conf = require( config.confpath );
		let obj;
		if( 'position' in arg ){
			let i;
			if( arg.position.length==6 ){
				for( i = 0; i<conf.objects.length; i++ ) if( conf.objects[i].id == arg.position ) break;
			}else{
				i = parseInt( arg.position ) - 1;
			}
			if( (i>=0) && (conf.objects.length > i) ){
				obj = conf.objects[i];
			}else{
				throw new Error('Указана позиция за границами списка');
			}
			let c = 'cd "' + obj.dst.dirname + '"; ls ' + obj.dst.basename.replace(/\<timestamp\>/, '*').replace(/\<datestamp\>/, '*') + '.tar.gz';
			let ssh;
			let files
			;
			if( obj.dst.proto=='ssh' ){
				ssh = 'ssh -p ' + obj.dst.port + ' -l ' +  obj.dst.user + ' ' + obj.dst.host + ' sh';
				let cmd = 'echo "' + c + '" | ' + ssh;
				files = await exec('sh', [ '-c',  cmd ]);
			}else{
				files = await exec('sh', [ '-c', c ]);
			};
			let result = files.map( f => { return { fullpath: obj.dst.dirname + '/' + f }; });
			for(let f of result){
				if(obj.dst.proto=='file'){
					let stats = await stat( f.fullpath );
					f.bytes = stats.size;
				}else{
					c = 'wc -c < "' + f.fullpath +'"';
					let cmd = 'echo "' + c + '" | ' + ssh;
					let output = await exec('sh', [ '-c',  cmd ]);
					f.bytes = parseInt( output[0] );
				}
				f.size = (f.bytes / ( 1024*1024 )).toFixed(1) + ' Mb';
				f.basename = path.basename(f.fullpath);
			}
			return result;
		}else{
			throw new Error('Необходимо указать удаляемую позицию или ключевое слово all для удаление всех объектов');
		}
		return;
	};
};
