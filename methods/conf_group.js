'use strict';

/*
* задать грппу [-]
*
*/

const fs = require('fs');
const { promisify } = require('util');
const writeFile = promisify(fs.writeFile);

//const exec = require('../modules/exec');
//const stringifyUrl = require ('../modules/stringifyUrl');
const getPositions = require ('../modules/getPositions');

const moment = require('moment');
moment.locale('ru');

module.exports = function ( config ) {
	const clear = (require('./clear'))(config);
	return async function (iarg, idata) {
		const arg = iarg || {};
		const data = idata || {};
		let conf = require( config.confpath );
		if( 'position' in arg ){
			if( 'groupname' in arg ){
				if( !arg.groupname.match(/^[a-zA-Z_]+$/) ) throw new Error( 'В названии группы присутствуют недопустимые символы. Доступны только символы: "a-zA-Z_". Введено: ' + arg.groupname );
				if( arg.groupname.match(/^all$/) ) throw new Error( 'Нельзя использовать ключевое слово all в качестве названия группы' );
				let list = getPositions( conf, arg.position );
				for( let obj of list ){
					obj.group = arg.groupname;
				}
				await writeFile( config.confpath, JSON.stringify(conf, null, 4), 'utf8');
				process.stdout.write('сохранено\n');
			}else{
				throw new Error('Необходимо указать имя группы');
			}
		}else{
			throw new Error('Необходимо указать позицию или ключевое слово all');
		}
		return;
	};
};

