'use strict';
/*
*  Вывод статистики [-]
*/

const getPositions     = require('../modules/getPositions');
const stringifyUrl     = require('../modules/stringifyUrl');
//const table            = require('../modules/table');
const { print_table }  = require('../modules/print_methods');

let task_ls;
module.exports = function ( config ) {
	task_ls = (require ('./task_ls'))(config);
	return async function (iarg, idata) {
		const arg = iarg || {};
		const data = idata || {};
		let conf = require( config.confpath );
		if( 'position' in arg ){
			let list = getPositions( conf, arg.position );
			await printStat(list);
			return;
		}else{
			throw new Error('Необходимо указать конкретную позицию или ключевое слово all для всех объектов');
		}
		return;
	};
};

function inmb(n){
	return (n / ( 1024*1024 )).toFixed(1) + ' Mb';
}

async function printStat(list){
	let bytes_total = 0;
	let filecount_total = 0;
	let max_total = 0;
	let rows = [];
	for( let t of list){
		let max_files = 0;
		max_files = max_files + t.plan.hourly;

		let days = Math.ceil(t.plan.hourly/24);
		max_files = max_files + Math.max( t.plan.daily - days, 0);
		Math.max( t.plan.daily, days);

		let weeks = Math.ceil( Math.max( t.plan.daily, days)/7);
		max_files = max_files + Math.max( t.plan.weekly - weeks, 0);

		let monthes = Math.ceil( Math.max(t.plan.weekly - weeks)/4);
		max_files = max_files + Math.max( t.plan.monthly - monthes, 0);

		let result =  await task_ls( {position: t.id } );
		let bytes = 0;
		let filecount = 0;
		for( let f of result ){
			bytes = bytes + f.bytes;
			filecount++;
		}
		let avgsize = Math.ceil(bytes/filecount);
		let max = max_files * avgsize;

		bytes_total = bytes_total + bytes;
		filecount_total = filecount_total + filecount;
		max_total = max_total + max;
		rows.push(
			{
				id: t.id,
				plan: t.plan.hourly +'-' + t.plan.daily + '-' + t.plan.weekly + '-' + t.plan.monthly,
				filecount: filecount.toString(),
				bytes: inmb(bytes),
				avgsize: inmb(avgsize),
				max_files: max_files.toString(),
				max: inmb(max),
				target: stringifyUrl( t.dst )
			}
		);
	}
	rows.push(null);
	rows.push(
		{
			id: '',
			plan: '',
			filecount: '',
			bytes: inmb(bytes_total),
			avgsize: '',
			max_files:'',
			max: inmb(max_total),
			target: ''
		}
	);
	print_table(
		[
			{column:'id', width: 7, title:'ID'},
			{column:'filecount', width: 6, title:'Файлов', type: 'currency'},
			{column:'bytes', width: 14, title:'Общий размер', type: 'currency'},
			{column:'avgsize', width: 14, title:'Ср. размер', type: 'currency'},
			{column:'plan', width: 10, title:'План'},
			{column:'max_files', width: 13, title:'Файлов (план)', type: 'currency'},
			{column:'max', width: 14, title:'Размер (план)', type: 'currency'},
			{column:'target', width: 70, title:'Цель'}
		],
		rows
	);
}
