'use strict';
/*
*  добавить объект [-]
*/

const fs = require('fs');
const path = require('path');
const { promisify } = require('util');
const writeFile = promisify(fs.writeFile);
const dns = require('dns');
const ESC         = '\x1b[';
const currentuser = require("os").userInfo().username;
const homedir = require('os').homedir();
const exec = require('../modules/exec');
const crypto = require('crypto');
const urlparser = require('url');

module.exports = function ( config ) {
	return async function (iarg, idata) {
		const arg = iarg || {};
		const data = idata || {};
		let conf = require( config.confpath );
		if(!conf.objects) conf.objects = [];
		if( !arg.src ) throw new Error('Необходимо указать источник');
		if( !arg.dst ) throw new Error('Необходимо указать приёмник');
		let src = await parseUrl(arg.src);
		let dst = await parseUrl(arg.dst);
		if(!src.local){
			try{
				let r = await lookup( src.host );
				src.ip = r.ip;
			}catch(err){
				process.stdout.write( ESC + '31m' +  'ПРЕДУПРЕЖДЕНИЕ: ' + ESC + '0m' +  err.message + '\n');
			}
		};
		if(!dst.local){
			try{
				let r = await lookup( dst.host );
				dst.ip = r.ip;
			}catch(err){
				process.stdout.write( ESC + '31m' +  'ПРЕДУПРЕЖДЕНИЕ: ' + ESC + '0m' +  err.message + '\n');
			}
		};
		let id=null;
		while(true){
			id = (id==null)?(arg.id || getRandomString(6)):getRandomString(6);
			if( !( id.match(/\d/) && id.match(/[a-f]/)) ) continue;
			if( !conf.objects.reduce( (prev, val)=>{ return prev || (val.id == id) }, false) ) break;
		}
		let checkmd5 = !!( arg.checkmd5 && (arg.checkmd5=='yes' || arg.checkmd5=='on' || arg.checkmd5=='1') );
		let delafter = !!( arg.delafter && (arg.delafter=='yes' || arg.delafter=='on' || arg.delafter=='1') );
		if( delafter && !src.local && ( src.proto == 'pgsql' ) ){
			process.stdout.write('ВНИМАНИЕ! Параметр --delafter  не может использоваться с источником типа pgsql. Будет проигнорировано.\n');
			delafter = false;
		}
		let a = (arg.plan || '24,7,4,12').split(/[.,:|-]/);
		let plan = { hourly:parseInt(a[0] || '24'), daily:parseInt(a[1] || '7'), weekly:parseInt(a[2] || '4'), monthly:parseInt(a[3] || '12') };
		conf.objects.push( { src, dst, id, plan, type: (src.local?'local':src.proto), group: (arg.group || ''), checkmd5, delafter  } );
		process.stdout.write('добавлено\n');
		await writeFile( config.confpath, JSON.stringify(conf, null, 4), 'utf8');
		process.stdout.write('сохранено\n');
		return;
	};
};

async function parseUrl( url ){
	let result = {};
	if( url.match(/:/) || url.match(/@/) || (!url.match(/^\//) && !url.match(/^\~/) ) ){
		result.local = false;
		let a;
		let pth;
		let host;
		let m;
		let parsed = urlparser.parse( url );
		let proto = ( parsed.protocol || '' ).replace(/:/g,'');
		if( [ 'file', 'ssh', 'postgres', 'pgsql', 'smb' ].indexOf(proto)<0 ) throw new Error('Неизвестный протокол: ' + proto);
		result.proto = proto;
		if(result.proto == 'postgres') result.proto = 'pgsql';
		result.host = parsed.hostname;
		if( parsed.port ){
			result.port = parseInt(parsed.port);
		}else{
			if( result.proto == 'ssh' ){
				result.port = 22;
			}else if( (result.proto == 'pgsql') || (result.proto == 'postgres') ){
				result.port = 5432;
			}
		}

		if( parsed.auth ){
			if( parsed.auth.match( /:/ ) ){
				let a = parsed.auth.split(/:/);
				result.user = a[0];
				a.splice(0, 1);
				result.pass = a.join(':');
			}else{
				result.user = parsed.auth;
			}
		}else{
			if( result.proto != 'file' ) result.user = currentuser;
		}

		result.basename = decodeURIComponent(path.basename( parsed.path )).trim();
		result.dirname = decodeURIComponent(path.dirname( parsed.path )).trim();
		if( result.dirname.match(/^\/\~/) ){
			let remotehomedir = await gethomedir(result);
			result.dirname = result.dirname.replace(/^\/\~/, remotehomedir);
		}

	}else{
		parseFileUrl( url, result )
	};
	if( result.dirname == '/' ) result.dirname = '';
	if( result.proto == 'file' ) result.local = true;
	return result;
}

function parseFileUrl( url, result ){
	result.proto = 'file'
	result.basename = decodeURIComponent(path.basename( url )).trim();
	result.dirname = decodeURIComponent(path.dirname( url )).trim() ;
	if( result.dirname.match(/^\~/ ) ) result.dirname = result.dirname.replace(/^\~/, homedir);
}

async function lookup( name ){
	return new Promise(function( resolve, reject ){
		dns.lookup(name, (err, address, family) => {
			if(err){
				reject( err );
			}else{
				resolve( {address:address, family:family} );
			}
		});
	});
}

async function gethomedir( urldata ){
	let aResult = await exec( 'ssh', [ '-p', urldata.port, urldata.user + '@' + urldata.host, 'pwd' ] );
	if( aResult.length > 0 ){
		return aResult[0];
	}else{
		throw new Error('Не найдено');
	}
}


/**
 * Возвращает случайную строку указанной длины
 * @param  {number} len Желаемая длина строки
 * @return {string}     Случайная строка
 */
function getRandomString(len){ return crypto.randomBytes( (parseInt(len)/2) || 16).toString('hex'); };
