'use strict';
/*
*  сохранить конфигурацию [-]
*/

const fs = require('fs');
const { promisify } = require('util');
const writeFile = promisify(fs.writeFile);

module.exports = function ( config ) {
	return async function () {
		let conf = require( config.confpath );
		if(!conf.objects) conf.objects = [];
		await writeFile( config.confpath, JSON.stringify(conf, null, 4), 'utf8');
		return;
	};
};
