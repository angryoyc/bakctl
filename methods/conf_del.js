'use strict';
/*
*  добавить объект [-]
*/

const fs = require('fs');
const { promisify } = require('util');
const writeFile = promisify(fs.writeFile);
const getPositions = require ('../modules/getPositions');

module.exports = function ( config ) {
	return async function (iarg, idata) {
		const arg = iarg || {};
		const data = idata || {};
		let conf = require( config.confpath );
		if( 'position' in arg ){
			if( arg.position=='all' ){
				conf.objects = [];
				process.stdout.write('очищено\n');
			}else{
				let list = getPositions( conf, arg.position );
				let index={};
				for( let p of list ) index[ p.id  ] = true;
				let n = conf.objects.length;
				conf.objects = conf.objects.filter(p=>{
					return  !(p.id in index);
				});
				process.stdout.write('исключено позиций: ' + (n - conf.objects.length)  + '\n');
			}
			await writeFile( config.confpath, JSON.stringify(conf, null, 4), 'utf8');
			process.stdout.write('сохранено\n');
		}else{
			throw new Error('Необходимо указать удаляемую позицию или ключевое слово all для удаление всех объектов');
		}
		return;
	};
};

