'use strict';

/*
* копирование удалённых дынных в локальный архивный файл [-]
*
* //ssh ${USER}@${HOST} "cd ${DIR};tar zpc ./{OBJ}" | cat > ${TARGETDIR}/{STOREFILENAME}.tar.gz
* STOREFILENAME и TARGETDIR могут содержать строку <timestamp> которая будет заменена на точное (до секунд) время создания архива
*
*/

const ESC     = '\x1b[';
const exec = require('../modules/exec');
const which = require('../modules/which');
const moment = require('moment');
moment.locale('ru');
const getPositions = require ('../modules/getPositions');
const crypto = require('crypto');
const fs = require('fs');
const { promisify } = require('util');
const writeFile = promisify(fs.writeFile);

module.exports = function ( config ) {
	const clear = (require('./clear'))(config);
	return async function ( iarg ) {
		const arg = iarg || {};
		let conf = require( config.confpath );
		const m = {};
		m.color = !!arg.color;
		let print1 = get_print1(m.color);
		let print2 = get_print2(m.color);
		let printerr = get_printerr(m.color);
		if( 'position' in arg ){
			let list = getPositions( conf, arg.position );
			let errors = [];
			for( let obj of list ){
				m.timestamp = moment().format('YYYY-MM-DD_HHmm');
				m.src_dirname = obj.src.dirname.replace(/ /g,'\\ ');
				m.src_basename = obj.src.basename.replace(/ /g,'\\ ');
				m.dst_dirname = obj.dst.dirname.replace(/ /g,'\\ ');
				m.dst_basename = obj.dst.basename.replace(/ /g,'\\ ').replace(/<timestamp>/, m.timestamp);
				m.tarname = m.dst_dirname + '/' + m.dst_basename + '.tar';
				m.archname = m.tarname + '.gz';
				if(obj.dst.proto=='ssh') m.dstssh = 'ssh -p ' + obj.dst.port + ' -l ' +  obj.dst.user + ' ' + obj.dst.host;
				if(obj.src.proto=='ssh') m.srcssh = 'ssh -p ' + obj.src.port + ' -l ' +  obj.src.user + ' ' + obj.src.host;

				process.stdout.write( '\n    Старт задания' + ' [' + obj.id + ']' + ' --> ' + obj.dst.proto + '://' + (obj.dst.host || '') + m.archname + '\n' );
				try{
					if( (obj.src.proto=='file') && (obj.dst.proto=='file')){ //file->file
						await exec('mkdir', [ '-p', m.dst_dirname ]);

						print1( 'проверка источника');
						let c3 = 'cd ' + m.src_dirname + '; find ./ -name \'' + m.src_basename +'\'';
						if( (await exec('sh', ['-c', c3 ])).length<=0) throw new Error('Отсутствуют объекты копирования');
						print2();

						print1( 'архивирование/сжатие');
						await exec('sh', [ '-c', 'cd "' + m.src_dirname + '";tar -cpzf ' + m.archname + ' ' + m.src_basename + '; exit $?' ]);
						print2();
						if( obj.checkmd5 ){
							print1( 'подсчёт контрольной суммы');
							await exec('sh', ['-c',  'md5sum ' + m.archname + ' > ' + m.archname + '.md5; exit $?'  ]);
							print2();
						}

						await clear( {position: obj.id, color: arg.color} );

					}else if( (obj.src.proto=='ssh') && (obj.dst.proto=='file')){ //ssh->file
						await exec('mkdir', [ '-p', m.dst_dirname ]);

						print1( 'проверка источника');
						let c3 = m.srcssh + ' "cd ' + m.src_dirname + '; find ./ -name \'' + m.src_basename +'\'"';
						if( (await exec('sh', ['-c', c3 ])).length<=0) throw new Error('Отсутствуют объекты копирования');
						print2();

						print1( 'архивирование/сжатие');
						let c2 = 'ssh -p ' + obj.src.port + ' -l ' +  obj.src.user + ' ' + obj.src.host + ' \'cd "' + m.src_dirname + '";tar zpc ./' + m.src_basename + (obj.checkmd5?' ./' + m.src_basename + '.md5':'') + '\' | cat > "' + m.dst_dirname +'/' + m.dst_basename + '.tar.gz"' ;
						await exec('sh', ['-c', c2 ]);
						print2();

						if( obj.checkmd5 ){
							print1( 'подсчёт контрольной суммы');
							await exec('sh', ['-c',  'md5sum ' + m.archname + ' > ' + m.archname + '.md5; exit $?'  ]);
							print2();
						}

						await clear( {position: obj.id, color: arg.color} );
						await setlaststate({config, conf, id: obj.id, state: 'reserved'})

					}else if( (obj.src.proto=='file') && (obj.dst.proto=='ssh')){ //file->ssh

						print1( 'проверка');
						if( !(obj.dst.host && m.share && obj.dst.pass, obj.dst.user, m.tempdir, obj.dst.basename) ) throw new Error('Не все параметры заданы корректно');
						await which(['sh', 'tar', 'cat', 'find' ], true);
						print2();

						print1( 'проверка источника');
						let c3 = 'cd ' + m.src_dirname + '; find ./ -name \'' + m.src_basename +'\'';
						if( (await exec('sh', ['-c', c3 ])).length<=0) throw new Error('Отсутствуют объекты копирования');
						print2();

						print1( 'архивирование/сжатие');
						let cmd = 'cd "' + m.src_dirname + '"; tar zpc ./' + m.src_basename + (obj.checkmd5?' ./' + m.src_basename + '.md5':'') + ' | ssh -p ' + obj.dst.port + ' -l ' +  obj.dst.user + ' ' + obj.dst.host + ' \'cat > "' + m.dst_dirname +'/' + m.dst_basename + '.tar.gz"\'';
						await exec('sh', ['-c', cmd ]);
						print2();

						if( obj.checkmd5 ){
							print1( 'подсчёт контрольной суммы');
							await exec('sh', ['-c', 'ssh -p ' + obj.dst.port + ' -l ' +  obj.dst.user + ' ' + obj.dst.host + ' \'md5sum "' + m.archname + '" > "' + m.archname + '.md5"\'' ]);
							print2();
						}

						await clear( {position: obj.id, color: arg.color} );
						await setlaststate({config, conf, id: obj.id, state: 'reserved'})

					}else if( (obj.src.proto=='file') && (obj.dst.proto=='smb')){ //file->smb

						print1( 'проверка');
						if( !(obj.dst.host && m.share && obj.dst.pass, obj.dst.user, m.tempdir, obj.dst.basename) ) throw new Error('Не все параметры заданы корректно');
						if( obj.checkmd5 ) await which(['md5sum'], true);
						await which(['sh', 'tar', 'rm', 'mkdir', 'find', 'smbclient' ], true);
						print2();

						print1( 'подготовка');
						m.tempdir = '/tmp/' + getRandomString(32);
						m.temparch = m.tempdir +'/' + m.dst_basename + '.tar.gz';
						let a = obj.dst.dirname.split('/').filter( s=>{ return s; } );
						m.share = a.shift();
						m.path  = a.join('/');
						m.smb = [ '\\\\' + obj.dst.host + '\\' + m.share, obj.dst.pass, '-U', obj.dst.user, '-c' ];
						await exec('sh', [ '-c', 'mkdir -p ' + m.tempdir ]);
						print2();

						print1( 'проверка источника');
						let c3 = 'cd ' + m.src_dirname + '; find ./ -name \'' + m.src_basename +'\'';
						if( (await exec('sh', ['-c', c3 ])).length<=0) throw new Error('Отсутствуют объекты копирования');
						print2();

						print1( 'архивирование/сжатие');
						let cmd = 'cd "' + m.src_dirname + '"; tar zpcf "' + m.temparch + '" ./' + m.src_basename;
						await exec('sh', ['-c', cmd ]);
						print2();

						let md5sum;
						if( obj.checkmd5 ){
							print1( 'подсчёт контрольной суммы');
							md5sum = await exec('md5sum', [ m.temparch ]);
							md5sum = (md5sum[0].split(/ +/).shift()) + ' ./' + m.dst_basename + '.tar.gz';
							await writeFile( m.temparch + '.md5', md5sum, 'utf8');
							print2();
						}

						print1( 'копирование на удалённый ресурс');
						try{
							await exec('smbclient', m.smb.concat([ 'cd "' + m.path + '";lcd "' + m.tempdir + '";put "' + (m.dst_basename + '.tar.gz') + '"' ]));
						}catch(err){
							smb_error(err);
						}
						if( obj.checkmd5 ){
							try{
								await exec('smbclient', m.smb.concat([ 'cd "' + m.path + '";lcd "' + m.tempdir + '";put "' + (m.dst_basename + '.tar.gz.md5') + '"' ]));
							}catch(err){
								smb_error(err);
							}
						}
						print2();

						if( obj.checkmd5 ){
							print1( 'проверка контрольной суммы ' + md5sum.split(/ +/).shift() );
							await exec('rm', [ m.temparch ]);
							try{
								await exec('smbclient', m.smb.concat([ 'cd "' + m.path + '";lcd "' + m.tempdir + '";get "' + (m.dst_basename + '.tar.gz') + '"' ]));
							}catch(err){
								smb_error(err);
							}
							let md5sum_remote = await exec('md5sum', [ m.temparch ]);
							md5sum_remote = (md5sum_remote[0].split(/ +/).shift()) + ' ./' + m.dst_basename + '.tar.gz';
							if( md5sum_remote != md5sum ) throw new Error('Контрольные суммы не совпали');
							print2();
						}

						print1( 'подметаем' );
						await exec('rm', ['-r', m.tempdir]);
						print2();

						await clear( {position: obj.id, color: arg.color} );
						await setlaststate({config, conf, id: obj.id, state: 'reserved'})

					}else if( (obj.src.proto=='ssh') && (obj.dst.proto=='ssh')){ //ssh->ssh

						print1( 'подготовка');
						m.tempdir = '/tmp/' + getRandomString(32);
						await exec('sh', [ '-c', 'mkdir -p ' + m.tempdir ]);
						m.temparch = m.tempdir +'/' + m.dst_basename + '.tar.gz';
						print2();

						print1( 'проверка источника');
						let c3 = m.srcssh + ' "cd ' + m.src_dirname + '; find ./ -name \'' + m.src_basename +'\'"';
						if( (await exec('sh', ['-c', c3 ])).length<=0) throw new Error('Отсутствуют объекты копирования');
						print2();

						print1( 'архивирование/сжатие');
						let c2 = 'ssh -p ' + obj.src.port + ' -l ' +  obj.src.user + ' ' + obj.src.host + ' \'cd "' + m.src_dirname + '";tar zpc ./' + m.src_basename + (obj.checkmd5?' ./' + m.src_basename + '.md5':'') + '\' | cat > "' + m.temparch + '"' ;
						await exec('sh', ['-c', c2 ]);
						print2();

						print1( 'копирование на целевой ресурс');
						await exec('sh', ['-c', 'cat "' + m.temparch + '" | ' + m.dstssh + ' \'cat >"' + m.archname + '"\'']);
						print2();

						if( obj.checkmd5 ){
							print1( 'подсчёт контрольной суммы');
							await exec('sh', ['-c', 'ssh -p ' + obj.dst.port + ' -l ' +  obj.dst.user + ' ' + obj.dst.host + ' \'md5sum "' + m.archname + '" > "' + m.archname + '.md5"\'' ]);
							print2();
						}

						print1( 'подметаем' );
						await exec('rm', ['-r', m.tempdir]);
						print2();

						await clear( {position: obj.id, color: arg.color} );
						await setlaststate({config, conf, id: obj.id, state: 'reserved'})

					}else if( (obj.src.proto=='ssh') && (obj.dst.proto=='smb')){ //ssh->smb

						print1( 'подготовка');
						m.tempdir = '/tmp/' + getRandomString(32);
						await exec('sh', [ '-c', 'mkdir -p ' + m.tempdir ]);
						m.temparch = m.tempdir +'/' + m.dst_basename + '.tar.gz';
						let a = obj.dst.dirname.split('/').filter( s=>{ return s; } );
						m.share = a.shift();
						m.path  = a.join('/');
						m.smb = [ '\\\\' + obj.dst.host + '\\' + m.share, obj.dst.pass, '-U', obj.dst.user, '-c' ];
						print2();

						print1( 'проверка источника');
						let c3 = m.srcssh + ' "cd ' + m.src_dirname + '; find ./ -name \'' + m.src_basename +'\'"';
						if( (await exec('sh', ['-c', c3 ])).length<=0) throw new Error('Отсутствуют объекты копирования');
						print2();

						print1( 'архивирование/сжатие');
						let c2 = 'ssh -p ' + obj.src.port + ' -l ' +  obj.src.user + ' ' + obj.src.host + ' \'cd "' + m.src_dirname + '";tar zpc ./' + m.src_basename + (obj.checkmd5?' ./' + m.src_basename + '.md5':'') + '\' | cat > "' + m.temparch + '"' ;
						await exec('sh', ['-c', c2 ]);
						print2();

						let md5sum;
						if( obj.checkmd5 ){
							print1( 'подсчёт контрольной суммы');
							md5sum = await exec('md5sum', [ m.temparch ]);
							md5sum = (md5sum[0].split(/ +/).shift()) + ' ./' + m.dst_basename + '.tar.gz';
							await writeFile( m.temparch + '.md5', md5sum, 'utf8');
							print2();
						}

						print1( 'копирование на удалённый ресурс');
						try{
							await exec('smbclient', m.smb.concat([ 'cd "' + m.path + '";lcd "' + m.tempdir + '";put "' + (m.dst_basename + '.tar.gz') + '"' ]));
						}catch(err){
							smb_error(err);
						}
						if( obj.checkmd5 ){
							try{
								await exec('smbclient', m.smb.concat([ 'cd "' + m.path + '";lcd "' + m.tempdir + '";put "' + (m.dst_basename + '.tar.gz.md5') + '"' ]));
							}catch(err){
								smb_error(err);
							}
						}
						print2();

						if( obj.checkmd5 ){
							print1( 'проверка контрольной суммы ' + md5sum.split(/ +/).shift() );
							await exec('rm', [ m.temparch ]);
							try{
								await exec('smbclient', m.smb.concat([ 'cd "' + m.path + '";lcd "' + m.tempdir + '";get "' + (m.dst_basename + '.tar.gz') + '"' ]));
							}catch(err){
								smb_error(err);
							}
							let md5sum_remote = await exec('md5sum', [ m.temparch ]);
							md5sum_remote = (md5sum_remote[0].split(/ +/).shift()) + ' ./' + m.dst_basename + '.tar.gz';
							if( md5sum_remote != md5sum ) throw new Error('Контрольные суммы не совпали');
							print2();
						}

						print1( 'подметаем' );
						await exec('rm', ['-r', m.tempdir]);
						print2();

						await clear( {position: obj.id, color: arg.color} );
						await setlaststate({config, conf, id: obj.id, state: 'reserved'})

					}else if( (obj.src.proto=='smb') && (obj.dst.proto=='smb')){ //smb->smb

						print1( 'подготовка');
						m.tempdir = '/tmp/' + getRandomString(32);
						await exec('sh', [ '-c', 'mkdir -p ' + m.tempdir ]);
						m.tempdir2 = '/tmp/' + getRandomString(32);
						await exec('sh', [ '-c', 'mkdir -p ' + m.tempdir2 ]);
						m.temparch = m.tempdir2 +'/' + m.dst_basename + '.tar.gz';
						let a = obj.dst.dirname.split('/').filter( s=>{ return s; } );
						m.dst_share = a.shift();
						m.dst_path  = a.join('/');
						m.dst_smb = [ '\\\\' + obj.dst.host + '\\' + m.dst_share, obj.dst.pass, '-U', obj.dst.user, '-c' ];
						let b = obj.src.dirname.split('/').filter( s=>{ return s; } );
						m.src_share = b.shift();
						m.src_path  = b.join('/');
						m.src_smb = [ '\\\\' + obj.src.host + '\\' + m.src_share, obj.src.pass, '-U', obj.src.user, '-c' ];
						print2();

						print1( 'проверка');
						if( !(obj.src.host && m.share && obj.src.pass, obj.src.user, m.tempdir, obj.src.basename) ) throw new Error('Не все параметры заданы корректно');
						await which(['sh', 'tar', 'rm','smbclient'], true);
						if( obj.checkmd5 ) await which(['md5sum'], true);
						print2();

						print1( 'копирование на локальный ресурс');
						try{
							await exec('smbclient', m.src_smb.concat( [ 'prompt OFF;recurse ON;cd "' + m.src_path + '";lcd ' + m.tempdir  + '; mget "' + obj.src.basename + '"' ]) );
						}catch(err){
							smb_error(err);
						}
						print2();

						print1( 'архивирование/сжатие');
						await exec('sh', [ '-c', 'cd "' + m.tempdir + '";tar -cpzf "' + m.temparch + '" .; exit $?' ]);
						print2();

						// ПЕРЕНОС НА УДАЛЁННЫЙ SMB ресурс

						let md5sum;

						if( obj.checkmd5 ){
							print1( 'подсчёт контрольной суммы' );
							md5sum = await exec('md5sum', [ m.temparch ]);
							md5sum = (md5sum[0].split(/ +/).shift()) + ' ./' + m.dst_basename + '.tar.gz';
							await writeFile( m.temparch + '.md5', md5sum, 'utf8');
							print2();
						}

						print1( 'копирование на удалённый ресурс' );
						try{
							await exec('smbclient', m.dst_smb.concat([ 'cd "' + m.dst_path + '";lcd "' + m.tempdir2 + '";put "' + (m.dst_basename + '.tar.gz') + '"' ]));
						}catch(err){
							smb_error(err);
						}
						if( obj.checkmd5 ){
							try{
								await exec('smbclient', m.dst_smb.concat([ 'cd "' + m.dst_path + '";lcd "' + m.tempdir2 + '";put "' + (m.dst_basename + '.tar.gz.md5') + '"' ]));
							}catch(err){
								smb_error(err);
							}
						}
						print2();

						if( obj.checkmd5 ){
							print1( 'проверка контрольной суммы ' + md5sum.split(/ +/).shift() );
							await exec('rm', [ m.temparch ]);
							try{
								await exec('smbclient', m.dst_smb.concat([ 'cd "' + m.dst_path + '";lcd "' + m.tempdir2 + '";get "' + (m.dst_basename + '.tar.gz') + '"' ]));
							}catch(err){
								smb_error(err);
							}
							let md5sum_remote = await exec('md5sum', [ m.temparch ]);
							md5sum_remote = (md5sum_remote[0].split(/ +/).shift()) + ' ./' + m.dst_basename + '.tar.gz';
							if( md5sum_remote != md5sum ) throw new Error('Контрольные суммы не совпали');
							print2();
						}

						print1( 'подметаем' );
						await exec('rm', ['-r', m.tempdir]);
						await exec('rm', ['-r', m.tempdir2]);
						print2();

						await clear( {position: obj.id, color: m.color} );
						await setlaststate({config, conf, id: obj.id, state: 'reserved'})

					}else if( (obj.src.proto=='smb') && (obj.dst.proto=='ssh')){ //smb->ssh
						print1( 'подготовка');
						m.tempdir = '/tmp/' + getRandomString(32);
						await exec('sh', [ '-c', 'mkdir -p ' + m.tempdir ]);
						m.tempdir2 = '/tmp/' + getRandomString(32);
						await exec('sh', [ '-c', 'mkdir -p ' + m.tempdir2 ]);
						m.temparch = m.tempdir +'/' + m.dst_basename + '.tar.gz';
						let a = obj.src.dirname.split('/').filter( s=>{ return s; } );
						m.share = a.shift();
						m.path  = a.join('/');
						print2();

						print1( 'проверка');
						if( !(obj.src.host && m.share && obj.src.pass, obj.src.user, m.tempdir, obj.src.basename) ) throw new Error('Не все параметры заданы корректно');
						await which(['sh', 'tar', 'rm','smbclient'], true);
						if( obj.checkmd5 ) await which(['md5sum'], true);
						print2();

						print1( 'копирование на локальный ресурс');
						try{
							await exec('smbclient', [ '\\\\' + obj.src.host + '\\' + m.share, obj.src.pass, '-U', obj.src.user, '-c', 'prompt OFF;recurse ON;cd "' + m.path + '";lcd ' + m.tempdir  + '; mget ' + obj.src.basename ]);
						}catch(err){
							smb_error(err);
						}
						print2();

						print1( 'архивирование/сжатие');
						await exec('sh', [ '-c', 'cd "' + m.tempdir + '";tar -cpzf "' + m.tempdir2 + '/' + m.dst_basename + '.tar.gz' + '" .; exit $?' ]);
						print2();

						// ПЕРЕНОС НА УДАЛЁННЫЙ SSH ресурс
						print1( 'копируем на целевой ресурс' );
						await exec('sh', ['-c', 'cat "' + m.tempdir2 + '/' + m.dst_basename + '.tar.gz' + '" | ' + m.dstssh + ' \'cat >"' + m.archname + '"\'; exit $?']);
						print2();

						print1( 'подметаем' );
						await exec('rm', ['-r', m.tempdir]);
						await exec('rm', ['-r', m.tempdir2]);
						print2();

						if( obj.checkmd5 ){
							print1( 'подсчёт контрольной суммы');
							await exec('sh', ['-c', 'ssh -p ' + obj.dst.port + ' -l ' +  obj.dst.user + ' ' + obj.dst.host + ' \'md5sum "' + m.archname + '" > "' + m.archname + '.md5"\'' ]);
							print2();
						}

						await clear( {position: obj.id, color: m.color} );
						await setlaststate({config, conf, id: obj.id, state: 'reserved'})

					}else if( (obj.src.proto=='smb') && (obj.dst.proto=='file')){ //smb->file
						print1( 'подготовка');
						m.tempdir = '/tmp/' + getRandomString(32);
						await exec('sh', [ '-c', 'mkdir -p ' + m.tempdir ]);
						m.temparch = m.tempdir +'/' + m.dst_basename + '.tar.gz';
						let a = obj.src.dirname.split('/').filter( s=>{ return s; } );
						m.share = a.shift();
						m.path  = a.join('/');
						print2();

						print1( 'проверка');
						if( !(obj.src.host && m.share && obj.src.pass, obj.src.user, m.tempdir, obj.src.basename) ) throw new Error('Не все параметры заданы корректно');
						await which(['sh', 'tar', 'rm','smbclient'], true);
						if( obj.checkmd5 ) await which(['md5sum'], true);
						print2();

						print1( 'копирование на локальный ресурс');
						try{
							await exec('smbclient', [ '\\\\' + obj.src.host + '\\' + m.share, obj.src.pass, '-U', obj.src.user, '-c', 'prompt OFF;recurse ON;cd "' + m.path + '";lcd ' + m.tempdir  + '; mget ' + obj.src.basename ]);
						}catch(err){
							smb_error(err);
						}
						print2();

						print1( 'архивирование/сжатие');
						await exec('sh', [ '-c', 'cd "' + m.tempdir + '";tar -cpzf "' + m.archname + '" .; exit $?' ]);
						print2();

						if( obj.checkmd5 ){
							print1( 'подсчёт контрольной суммы');
							await exec('sh', ['-c',  'md5sum ' + m.archname + ' > ' + m.archname + '.md5; exit $?'  ]);
							print2();
						}

						print1( 'подметаем' );
						await exec('rm', ['-r', m.tempdir]);
						print2();

						await clear( {position: obj.id, color: arg.color} );
						await setlaststate({config, conf, id: obj.id, state: 'reserved'})

					}else if( (obj.src.proto=='pgsql') && (obj.dst.proto=='file')){ //pgsql->file
						m.tempdir = '/tmp/' + getRandomString(32);
						print1( 'подготовка');
						await exec('sh', [ '-c', 'mkdir -p ' + m.dst_dirname ]);
						await exec('sh', [ '-c', 'mkdir -p ' + m.tempdir ]);
						print2();

						let archname = await make_bak_from_pgsql( obj, m.dst_dirname, m );

						print1( 'подметаем' );
						await exec('rm', ['-r', m.tempdir]);
						print2();

						if( obj.checkmd5 ){
							print1( 'подсчёт контрольной суммы');
							await exec('sh', ['-c',  'md5sum ' + archname + ' > ' + archname + '.md5; exit $?'  ]);
							print2();
						}

						await clear( {position: obj.id, color: arg.color} );
						await setlaststate({config, conf, id: obj.id, state: 'reserved'})

					}else if( (obj.src.proto=='pgsql') && (obj.dst.proto=='ssh') ){ //pgsql->ssh
						print1( 'подготовка');
						m.tempdir = '/tmp/' + getRandomString(32);
						m.tempdir2 = '/tmp/' + getRandomString(32);
						await exec('sh', [ '-c', 'mkdir -p ' + m.tempdir ]);
						await exec('sh', [ '-c', 'mkdir -p ' + m.tempdir2 ]);
						let cmd = 'mkdir -p "' + m.dst_dirname + '"';
						await exec('sh', [ '-c', 'echo \'' +cmd + '\' | ' + m.dstssh ]);
						print2();

						let archname = await make_bak_from_pgsql( obj, m.tempdir2, m );

						print1( 'копируем на целевой ресурс' );
						await exec('sh', ['-c', 'cat "' + archname + '" | ' + m.dstssh + ' \'cat >"' + m.archname + '"\'']);
						print2();

						print1( 'подметаем' );
						await exec('rm', ['-r', m.tempdir]);
						await exec('rm', ['-r', m.tempdir2]);
						print2();

						if( obj.checkmd5 ){
							print1( 'подсчёт контрольной суммы');
							await exec('sh', ['-c', 'ssh -p ' + obj.dst.port + ' -l ' +  obj.dst.user + ' ' + obj.dst.host + ' \'md5sum "' + m.archname + '" > "' + m.archname + '.md5"\'' ]);
							print2();
						}

						await clear( {position: obj.id, color: m.color} );
						await setlaststate({config, conf, id: obj.id, state: 'reserved'})

					}else if( (obj.src.proto=='pgsql') && (obj.dst.proto=='smb')){ //pgsql->smb

						print1( 'подготовка');
						m.tempdir = '/tmp/' + getRandomString(32);
						m.tempdir2 = '/tmp/' + getRandomString(32);
						await exec('sh', [ '-c', 'mkdir -p ' + m.tempdir ]);
						await exec('sh', [ '-c', 'mkdir -p ' + m.tempdir2 ]);
						m.temparch = m.tempdir2 +'/' + m.dst_basename + '.tar.gz';

						let a = obj.dst.dirname.split('/').filter( s=>{ return s; } );
						m.share = a.shift();
						m.path  = a.join('/');
						m.smb = [ '\\\\' + obj.dst.host + '\\' + m.share, obj.dst.pass, '-U', obj.dst.user, '-c' ];
						print2();

						await make_bak_from_pgsql( obj, m.tempdir2, m );

						let md5sum;
						if( obj.checkmd5 ){
							print1( 'подсчёт контрольной суммы');
							md5sum = await exec('md5sum', [ m.temparch ]);
							md5sum = (md5sum[0].split(/ +/).shift()) + ' ./' + m.dst_basename + '.tar.gz';
							await writeFile( m.temparch + '.md5', md5sum, 'utf8');
							print2();
						}

						print1( 'копирование на удалённый ресурс');
						try{
							await exec('smbclient', m.smb.concat([ 'cd "' + m.path + '";lcd "' + m.tempdir2 + '";put "' + (m.dst_basename + '.tar.gz') + '"' ]));
						}catch(err){
							smb_error(err);
						}
						if( obj.checkmd5 ){
							try{
								await exec('smbclient', m.smb.concat([ 'cd "' + m.path + '";lcd "' + m.tempdir2 + '";put "' + (m.dst_basename + '.tar.gz.md5') + '"' ]));
							}catch(err){
								smb_error(err);
							}
						}
						print2();

						if( obj.checkmd5 ){
							print1( 'проверка контрольной суммы ' + md5sum.split(/ +/).shift() );
							await exec('rm', [ m.temparch ]);
							try{
								await exec('smbclient', m.smb.concat([ 'cd "' + m.path + '";lcd "' + m.tempdir2 + '";get "' + (m.dst_basename + '.tar.gz') + '"' ]));
							}catch(err){
								smb_error(err);
							}
							let md5sum_remote = await exec('md5sum', [ m.temparch ]);
							md5sum_remote = (md5sum_remote[0].split(/ +/).shift()) + ' ./' + m.dst_basename + '.tar.gz';
							if( md5sum_remote != md5sum ) throw new Error('Контрольные суммы не совпали');
							print2();
						}

						print1( 'подметаем' );
						await exec('rm', ['-r', m.tempdir]);
						await exec('rm', ['-r', m.tempdir2]);
						print2();

						await clear( {position: obj.id, color: arg.color} );
						await setlaststate({config, conf, id: obj.id, state: 'reserved'})
					}else{
						const message = 'Неизвестный тип задания ' + obj.src.proto + ' -> ' + obj.dst.proto;
						await setlaststate({config, conf, id: obj.id, state: 'error', message});
						throw new Error( message );
					}
					print1( 'закончено' );
					print2();
				}catch(err){
					await setlaststate({config, conf, id: obj.id, state: 'error', message: err.message })
					printerr( err.message );
					errors.push( 'Задача id: ' + obj.id + ' произошла ошибка: ' + err.message );
				}
			}
			return errors;
		}else{
			throw new Error('Необходимо указать удаляемую позицию или ключевое слово all для удаление всех объектов');
		}
	};
};


async function make_bak_from_pgsql(obj, target, m){
	let print1 = get_print1( m.color );
	let print2 = get_print2( m.color );
	let tarname = target + '/' + m.dst_basename + '.tar';

	let schemas = m.src_dirname.split('/').filter( n => {return n;});

	if(schemas.length>0){
		for(let i in schemas){
			let schema = schemas[i];

			print1( 'выгрузка схемы ' + schema );
			await exec('pg_dump', ['-h', obj.src.host, '-p', obj.src.port, '-n', schema, '-c', '-U', obj.src.user, m.src_basename, '-F', 'p', '-f', (m.tempdir + '/' + schema) + '.sql' ]);
			await exec('tar', [ '-' + (i==0?'c':'r') + 'pf', tarname , '--directory', m.tempdir, './' + schema + '.sql']);
			await exec('rm',  [(m.tempdir + '/' + schema)  + '.sql']);
			print2();
		}
	}else{
		print1( 'выгрузка базы ' + m.src_basename );
		await exec('pg_dump', ['-h', obj.src.host, '-p', obj.src.port, '-c', '-U', obj.src.user, m.src_basename, '-F', 'p', '-f', (m.tempdir + '/' + m.src_basename) + '.sql' ]);
		print2();

		print1( 'архивирование в ' + tarname );
		await exec('tar', [ '-cpf', tarname, '--directory', m.tempdir, './' + m.src_basename + '.sql']);
		await exec('rm',  [(m.tempdir + '/' + m.src_basename)  + '.sql']);
		print2();
	}

	print1( 'сжатие' );
	await exec('gzip', [ tarname ]);
	print2();

	return (tarname + '.gz');
}


/**
 * Возвращает случайную строку указанной длины
 * @param  {number} len Желаемая длина строки
 * @return {string}     Случайная строка
 */
function getRandomString(len){ return crypto.randomBytes( (parseInt(len)/2) || 16).toString('hex'); };


/******************** TRASH ********************/
//await exec('sh', ['-c', 'pg_dump -h ' + obj.src.host + ' -p ' + obj.src.port + ' -n ' + schema  + ' -c -U ' +  obj.src.user + ' ' + src_basename + ' -F p -f ' + (tempdir + '/' + schema)  + '.sql' ]);
//await exec('sh', ['-c', 'cd "' + tempdir +'"; tar -' + (i==0?'c':'r') + 'pf "' + dst_dirname +'/' + dst_basename + '.tar" ./' + schema + '.sql']);
//await exec('sh',  ['-c', 'cd ' + tempdir]);


function get_print1(color){
	if(color){
		return function(s){
			process.stdout.write('      ' + ESC + '32m' + ' ✔ ' + ESC + '0m' + ESC + '2m' + s + ' ... ' + ESC + '0m');
		}
	}else{
		return function(s){
			process.stdout.write('       ✔ ' + s + ' ... ');
		}
	}
}
function get_print2(color){
	if(color){
		return function(s){
			process.stdout.write( '[ ' + ESC + '32m' + 'ok' + ESC + '0m' + ' ] ' + '\n' );
		}
	}else{
		return function(s){
			process.stdout.write( '[ ok ] ' + '\n' );
		}
	}
}

function get_printerr(color){
	if(color){
		return function(s){
			process.stdout.write(' '.repeat(7) +  ESC + '31m' +  'ERR: ' + ESC + '0m' + s + '\n');
		}
	}else{
		return function(s){
			process.stdout.write(' '.repeat(7) + 'ERR: ' + s + '\n');
		}
	}
}

function smb_error(e){
	let mess;
	if(e.rows && Array.isArray(e.rows)){
		mess=(e.rows.filter(r=>{
			return r.match(/NT_STATUS/);
		}))[0];
	}else{
		mess = e.message;
	}
	throw new Error(mess);
}

async function setlaststate({config, conf, id=0, state='unknown', message=''}){
	const task = (conf.objects.filter(o=>{
		return o.id == id;
	}))[0];
	if(task){
		task.laststate = state;
		if( state == 'error' ) task.message = message;
		await writeFile( config.confpath, JSON.stringify(conf, null, 4), 'utf8');
	}
}