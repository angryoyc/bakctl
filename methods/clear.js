'use strict';

/*
* Удаление старых копий в соответствии с политикой [-]
*
*/

const ESC     = '\x1b[';
const fs = require('fs');
const { promisify } = require('util');
const stat = promisify(fs.stat);
const unlink = promisify(fs.unlink);

const exec = require('../modules/exec');
const moment = require('moment');
moment.locale('ru');

const getPositions = require('../modules/getPositions');

module.exports = function ( config ) {
	return async function ( iarg ) {
		const arg = iarg || {};
		let conf = require( config.confpath );
		if( 'position' in arg ){
			let list = getPositions( conf, arg.position );
			for( let obj of list ){
				if( (obj.dst.proto=='file') || (obj.dst.proto=='ssh') || (obj.dst.proto=='smb') ){
					let c, share, path;
					if(obj.dst.proto=='smb'){
						let a = obj.dst.dirname.split('/').filter( s=>{ return s; } );
						share = a.shift();
						path  = a.join('\\');
					}else{
						path = obj.dst.dirname;
					}
					c = 'cd "' + path + '"; ls ' + (obj.dst.basename.match(/\<timestamp\>/)?obj.dst.basename:obj.dst.basename + '<timestamp>').replace(/<timestamp>/, '*') + '.tar.gz';
					let files;
					let ssh;
					if( obj.dst.proto=='ssh' ){
						ssh = 'ssh -p ' + obj.dst.port + ' -l ' +  obj.dst.user + ' ' + obj.dst.host + ' sh';
						let cmd = 'echo "' + c + '" | ' + ssh;
						files = await exec('sh', [ '-c',  cmd ]);
					}else if(obj.dst.proto=='smb'){
						try{
							files = await exec('smbclient', [ '\\\\' + obj.dst.host + '\\' + share, obj.dst.pass, '-U', obj.dst.user, '-c', c ] );
							files = files
							.filter( l => {
								return l.match(/\d\d\d\d$/)
							})
							.map(  l => {
								return l.match(/^ +(.+?) +. +\d+ +... ... \d+ \d\d:\d\d:\d\d \d+$/)[1];
							});
						}catch(e){
							smb_error(e);
						}
					}else{
						files = await exec('sh', [ '-c', c ]);
					}
					if( files.length>0 ){
						files = files.map(function(l){
							return { filename:l };
						});
						for( let f of files ){
							let filename = path + '/' + f.filename;
							try{
								if(obj.dst.proto=='file'){
									f.size = ( await stat(filename) ).size;
								}else if(obj.dst.proto=='ssh'){
									let cmd = 'wc -c < "' + filename + '"';
									let result = await exec('sh', [ '-c', 'echo \'' + cmd + '\' | ' + ssh ]);
									f.size = parseInt( result[0] );
								}else if(obj.dst.proto=='smb'){
									let statdata = await exec('smbclient', [ '\\\\' + obj.dst.host + '\\' + share, obj.dst.pass, '-U', obj.dst.user, '-c', 'stat "' + filename +'"' ] );
									let l = (statdata.filter(l => {
										return l.match(/^Size: /);
									}))[0];
									f.size = parseInt( l.split(/ +/)[1] );
								}
							}catch(err){}
							try{ if( f.size<=0 )
								if(obj.dst.proto=='file'){
									await unlink( filename );
								}else if(obj.dst.proto=='smb'){
									await exec('smbclient', [ '\\\\' + obj.dst.host + '\\' + share, obj.dst.pass, '-U', obj.dst.user, '-c', 'rm "' + filename + '"' ] );
								}else if(obj.dst.proto=='ssh'){
									let cmd = 'rm "' + filename +'"';
									await exec('sh', ['-c', 'echo \'' + cmd + '\' | ' + ssh]);
								}
							}catch(err){}
						}

						files = files.filter( f=>{ return (f.size>0); });
						for(let crit of Object.keys(obj.plan) ){
							groupBy(files, crit, parseInt(obj.plan[crit]));
						}

						for( let f of files ){
							if( !f.ok ){
								try{
									let file =  path + '/' +f.filename;
									if( obj.dst.proto=='file' ){
										await unlink( file );
										try{ await unlink( file + '.md5' ) }catch(e){};
									}else if( obj.dst.proto=='smb' ){
										try{ await exec('smbclient', [ '\\\\' + obj.dst.host + '\\' + share, obj.dst.pass, '-U', obj.dst.user, '-c', 'rm "' + file +'"' ] ); }catch(e){}
										try{ await exec('smbclient', [ '\\\\' + obj.dst.host + '\\' + share, obj.dst.pass, '-U', obj.dst.user, '-c', 'rm "' + file + '.md5' +'"' ] ); }catch(e){}
									}else if( obj.dst.proto=='ssh' ){
										let cmd;
										cmd = 'rm "' + file +'"';
										try{ await exec( 'sh', ['-c', 'echo \'' + cmd + '\' | ' + ssh]); }catch(e){}
										cmd = 'rm "' + file +'.md5' +'"';
										try{ await exec( 'sh', ['-c', 'echo \'' + cmd + '\' | ' + ssh]); }catch(e){}
									}
									if( arg.color ){
										process.stdout.write('      ' + ESC + '32m' + ' ✔ ' + ESC + '0m' + ESC + '2m' + 'удалено: ' + file + ' ... ' + ESC + '0m' + '[ ' + ESC + '32m' + 'ok' + ESC + '0m' + ' ] ' + '\n');
									}else{
										process.stdout.write('      ' + ' ✔ ' + 'удалено: ' + file + ' ... ' + '[ ' + 'ok' + ' ] ' + '\n');
									}
								}catch(e){
									process.stdout.write( e.message + '\n' );
								}
							}
						}
					}
				}
			}
		}else{
			throw new Error('Необходимо указать удаляемую позицию или ключевое слово all для всех объектов');
		}
		return;
	};
};

function groupBy( files, crit, quant ){
	let groups={};
	for( let f of files ){
		let m;
		if( m = f.filename.match(/_(\d\d\d\d-\d\d-\d\d).(\d\d\d\d)/) ){
			let dt = m[ 1 ];
			let tm = m[ 2 ];
			if( crit == 'hourly' ){
				f.crit = dt + '_' + tm.substr(0, 2);
			}else if( crit == 'daily' ){
				f.crit = dt;
			}else if( crit == 'weekly' ){
				let a = dt.split('-');
				f.crit = a[0] + '-' + ('0' + parseInt(moment( m[1] ).week())).substr(-2);
			}else if( crit == 'monthly' ){
				let a = dt.split('-');
				f.crit = a[0] + '-' + a[1] ;
			}
		}
		if( !groups[f.crit] ) groups[f.crit] = { members:[] };
		groups[f.crit].members.push(f);
	}
	for( let crit in groups ){
		let g = groups[ crit ];
		g.members = g.members.sort((a,b)=>{
			if(a.filename>b.filename) return 1;
			if(a.filename<b.filename) return -1;
			if(a.filename=b.filename) return 0;
		});
	}
	for( let crit of Object.keys(groups).reverse().splice(0, quant) ){
		let g = groups[ crit ];
		if(g.members) g.members[g.members.length-1].ok = true;
	}
	return groups;
}

function smb_error(e){
	let mess;
	if(e.rows && Array.isArray(e.rows)){
		mess=(e.rows.filter(r=>{
			return r.match(/NT_STATUS/);
		}))[0];
	}else{
		mess = e.message;
	}
	throw new Error(mess);
}
