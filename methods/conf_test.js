'use strict';

/*
* задать грппу [-]
*
*/

const ESC     = '\x1b[';
const fs = require('fs');
const { promisify } = require('util');
const stat = promisify(fs.stat);
const unlink = promisify(fs.unlink);
const exec = require('../modules/exec');
const getPositions = require ('../modules/getPositions');
const moment = require('moment');
const dotpgpass = require('../modules/dotpgpass');
moment.locale('ru');
const crypto = require('crypto');
let conf_save;

module.exports = function ( config ) {
	conf_save = (require('../methods/conf_save'))(config);
	return async function (iarg) {
		const arg = iarg || {};
		let conf = require( config.confpath );
		let force = ('force' in arg)?!!arg.force:false;
		if( 'position' in arg ){
			const result = {};
			let list = getPositions( conf, arg.position);
			for( let j of list ){
				process.stdout.write('\n  ' + ESC + '4mЗадание' + ESC + '0m' + ' [' + j.id + ']' + ESC + '0m' + '\n');
				result[j.id] = { tests:[] }
				await test(j, result[j.id].tests, force);
			}
			return result;
		}else{
			throw new Error('Необходимо указать позицию или ключевое слово all');
		}
	};
};

async function test( resource, tests, force ){
	process.stdout.write('    Тест источника\n');
	if( resource.src.proto=='file' ){
		try{
			await file_src( resource.src, tests, force );
			resource.tested = true;
		}catch(e){
			resource.tested = false;
		}
	}else if( resource.src.proto=='ssh' ){
		try{
			await ssh_src( resource.src, tests, force );
			resource.tested = true;
		}catch(e){
			resource.tested = false;
		}
	}else if( resource.src.proto=='pgsql' ){
		try{
			await pgsql_src( resource.src, tests, force );
			resource.tested = true;
		}catch(e){
			resource.tested = false;
		}
	}else if( resource.src.proto=='smb' ){
		try{
			await smb_src( resource.src, tests );
		}catch(e){
			resource.tested = true;
			resource.tested = false;
		}
	}else{
		resource.tested = false;
		printTestResult({name:'Формат записи задачи', status:'err', message:'Незивесный тип задачи'});
	}
	process.stdout.write('    Тест приёмника\n');
	if( resource.dst.proto=='file' ){
		try{
			await file_dst( resource.dst, tests, force );
			resource.tested = true;
		}catch(e){
			resource.tested = false;
		}
	}else if( resource.dst.proto=='ssh' ){
		try{
			await ssh_dst( resource.dst, tests, force );
			resource.tested = true;
		}catch(e){
			resource.tested = false;
		}
	}else if( resource.dst.proto=='pgsql' ){
		try{
			await pgsql_dst( resource.dst, tests );
			resource.tested = true;
		}catch(e){
			resource.tested = false;
		}
	}else if( resource.dst.proto=='smb' ){
		try{
			await smb_dst( resource.dst, tests, force );
			resource.tested = true;
		}catch(e){
			resource.tested = false;
		}
	}else{
		resource.tested = false;
		printTestResult({name:'Формат записи задачи', status:'err', message:'Незивесный тип задачи'});
	}
}

async function smb_src( resource, tests ){
	tests.push( await smb_src_test0( resource ) );
	if(tests[tests.length-1].status=='err') throw new Error(tests[tests.length-1].message);
	tests.push( await smb_src_test1( resource ) );
	if(tests[tests.length-1].status=='err') throw new Error(tests[tests.length-1].message);
	tests.push( await smb_src_test2( resource ) );
	if(tests[tests.length-1].status=='err') throw new Error(tests[tests.length-1].message);
	return tests;
}

async function smb_dst( resource, tests, force ){
	tests.push( await smb_dst_test0( resource, force ) );
	if(tests[tests.length-1].status=='err') throw new Error(tests[tests.length-1].message);
	tests.push( await smb_dst_test1( resource, force ) );
	if(tests[tests.length-1].status=='err') throw new Error(tests[tests.length-1].message);
	tests.push( await smb_dst_test2( resource, force ) );
	if(tests[tests.length-1].status=='err') throw new Error(tests[tests.length-1].message);
	return tests;
}

async function pgsql_src( resource, tests, force ){
	tests.push( await pgsql_src_test1( resource ) );
	if(tests[tests.length-1].status=='err') throw new Error(tests[tests.length-1].message);
	tests.push( await pgsql_src_test2( resource, force ) );
	if(tests[tests.length-1].status=='err') throw new Error(tests[tests.length-1].message);
	tests.push( await pgsql_src_test3( resource) );
	if(tests[tests.length-1].status=='err') throw new Error(tests[tests.length-1].message);
	tests.push( await pgsql_src_test4( resource) );
	if(tests[tests.length-1].status=='err') throw new Error(tests[tests.length-1].message);
	tests.push( await pgsql_src_test5( resource) );
	if(tests[tests.length-1].status=='err') throw new Error(tests[tests.length-1].message);
	tests.push( await pgsql_src_test6( resource, tests[tests.length-1].schemas) );
	if(tests[tests.length-1].status=='err') throw new Error(tests[tests.length-1].message);
	tests.push( await pgsql_src_test7( resource, tests[tests.length-2].schemas) );
	if(tests[tests.length-1].status=='err') throw new Error(tests[tests.length-1].message);
	return tests;
}

async function pgsql_dst( resource, tests ){
	tests.push( await pgsql_dst_test1( resource ) );
	if(tests[tests.length-1].status=='err') throw new Error(tests[tests.length-1].message);
	return tests;
}


async function ssh_src( resource, tests ){
	tests.push( await ssh_src_test1( resource ) );
	if(tests[tests.length-1].status=='err') throw new Error(tests[tests.length-1].message);
	tests.push( await ssh_src_test2( resource ) );
	if(tests[tests.length-1].status=='err') throw new Error(tests[tests.length-1].message);
	return tests;
}

async function ssh_dst( resource, tests, force ){
	tests.push( await ssh_dst_test1( resource ) );
	if(tests[tests.length-1].status=='err') throw new Error(tests[tests.length-1].message);
	tests.push( await ssh_dst_test2( resource, force ) );
	if(tests[tests.length-1].status=='err') throw new Error(tests[tests.length-1].message);
	tests.push( await ssh_dst_test3( resource ) );
	if(tests[tests.length-1].status=='err') throw new Error(tests[tests.length-1].message);
	if(force) force;
	return tests;
}

async function file_src( resource, tests, force ){
	tests.push( await file_src_test1(resource, force) );
	if(tests[tests.length-1].status=='err') throw new Error(tests[tests.length-1].message);
	tests.push( await file_src_test2(resource) );
	if(tests[tests.length-1].status=='err') throw new Error(tests[tests.length-1].message);
	return tests;
}

async function file_dst( resource, tests, force ){
	tests.push( await file_dst_test1(resource, force) );
	if(tests[tests.length-1].status=='err') throw new Error(tests[tests.length-1].message);
	tests.push( await file_dst_test2(resource) );
	if(tests[tests.length-1].status=='err') throw new Error(tests[tests.length-1].message);
	tests.push( await file_dst_test3(resource) );
	if(tests[tests.length-1].status=='err') throw new Error(tests[tests.length-1].message);
	return tests;
}

async function file_src_test1( resource, force ){
	let test = {};
	test.name = 'Тест пути директории "' + resource.dirname + '"';
	let stats;
	try{
		stats = await stat( resource.dirname );
		if(stats) test.status = 'ok';
	}catch(err){
		if( (err.code == 'ENOENT') && force ){
			try{
				await exec('mkdir', ['-p', resource.dirname]);
				test.status = 'ok';
				test.message = 'Путь создан!';
			}catch(err){
				test.status = 'err';
				test.message = err.message;
			}
		}else{
			test.status = 'err';
			test.message = err.message;
		}
	}
	printTestResult( test );
	return test;
}

async function file_src_test2( resource ){
	let test = {};
	test.name = 'Путь является директорией ';
	let stats = await stat( resource.dirname );
	if(stats.isDirectory()){
		test.status = 'ok';
	}else{
		test.status = 'err';
		test.message = 'Путь не является директорией';
	}
	printTestResult( test );
	return test;
}

async function file_dst_test1( resource, force ){
	let test = {};
	test.name = 'Тест пути директории "' + resource.dirname + '"';
	let stats;
	try{
		stats = await stat( resource.dirname );
		if(stats) test.status = 'ok';
	}catch(err){
		if( (err.code == 'ENOENT') && force ){
			try{
				await exec('mkdir', ['-p', resource.dirname]);
				test.status = 'ok';
				test.message = 'Путь создан!';
			}catch(err){
				test.status = 'err';
				test.message = err.message;
			}
		}else{
			test.status = 'err';
			test.message = err.message;
		}
	}
	printTestResult( test );
	return test;
}

async function file_dst_test2( resource ){
	let test = {};
	test.name = 'Путь является директорией ';
	let stats = await stat( resource.dirname );
	if(stats.isDirectory()){
		test.status = 'ok';
	}else{
		test.status = 'err';
		test.message = 'Путь не является директорией';
	}
	printTestResult( test );
	return test;
}

async function file_dst_test3( resource ){
	let test = {};
	test.name = 'Права текущего пользователя в целевой директории';
	const tempfilename = resource.dirname + '/' + getRandomString(32);
	try{
		await exec('touch', [tempfilename]);
		test.status = 'ok';
		await unlink( tempfilename );
	}catch(err){
		test.status = 'err';
		test.message = err.message;
	}
	printTestResult( test );
	return test;
}

async function ssh_src_test1( resource ){
	let test = {};
	test.name = 'Наличие ssh доступа на удалённый сервер ' + resource.host;
	try{
		await exec('ssh', [ '-l', resource.user, '-p', resource.port, resource.host, 'ls', '-l', '/'] );
		test.status = 'ok';
	}catch(err){
		test.status = 'err';
		test.message = err.message;
	}
	printTestResult( test );
	return test;
}

async function ssh_dst_test1( resource ){
	let test = {};
	test.name = 'Наличие ssh доступа на удалённый сервер ' + resource.host;
	try{
		await exec('ssh', [ '-l', resource.user, '-p', resource.port, resource.host, 'ls', '-l', '/'] );
		test.status = 'ok';
	}catch(err){
		test.status = 'err';
		test.message = err.message;
	}
	printTestResult( test );
	return test;
}

async function ssh_dst_test2( resource, force ){
	let test = {};
	test.name = 'Наличие папки-источника на удалённом сервере ' + resource.host;
	try{
		let cmd = "echo \"if [[ -d '" + resource.dirname + "' ]]; then echo 1; else echo 2; fi\" | ssh -l " + resource.user + " -p " + resource.port + " " + resource.host + " bash";
		let result = await exec('bash', [ '-c', cmd ] );
		if(result[0] == '1'){
			test.status = 'ok';
		}else{
			if( force ){
				let cmd = "echo \"mkdir -p '" + resource.dirname + "';if [[ -d '" + resource.dirname + "' ]]; then echo 1; else echo 2; fi\" | ssh -l " + resource.user + " -p " + resource.port + " " + resource.host + " bash";
				let result = await exec('bash', [ '-c', cmd ] );
				if(result[0] == '1'){
					test.status = 'ok';
				}else{
					test.status = 'err';
					test.message = 'Директория "' + resource.dirname + '" приёмника не существует или не является директорией. Исправить не удалось.';
				}
			}else{
				test.status = 'err';
				test.message = 'Директория "' + resource.dirname + '" приёмника не существует или не является директорией';
			}
		}
	}catch(err){
		test.status = 'err';
		test.message = err.message;
	}
	printTestResult( test );
	return test;
}

async function ssh_dst_test3( resource ){
	let test = {};
	test.name = 'Проверка доступности директории на запись на удалённом сервере ' + resource.host;
	try{
		const fl = resource.dirname + '/' + getRandomString(32);
		// rm '" + fl + "';
		let cmd = "echo \"touch '" + fl + "'; if [[ -f '" + fl + "' ]]; then echo 1; rm '" + fl + "'; else echo 2; fi\" | ssh -l " + resource.user + " -p " + resource.port + " " + resource.host + " bash";
		let result = await exec('bash', [ '-c', cmd ] );
		if(result[0] == '1'){
			test.status = 'ok';
		}else{
			test.status = 'err';
			test.message = 'В директории-приёмнике недостаточно прав для создания файлов';
		}
	}catch(err){
		test.status = 'err';
		test.message = err.message;
	}
	printTestResult( test );
	return test;
}

async function ssh_src_test2( resource ){
	let test = {};
	test.name = 'Наличие на удалённом сервере подходящих объектов для копирования';
	try{
		let command = 'ssh -l ' + resource.user + ' -p ' + resource.port + ' ' + resource.host + ' ls "' + resource.dirname.replace(/ /g,'\\ ') + '/' + resource.basename.replace(/ /g,'\\ ') + '"';
		let param = [ '-c', command ];
		let list = await exec('sh',  param);
		if(list.length>0){
			test.status = 'ok';
			test.message = 'Файлов для копирования: ' + list.length;
		}else{
			test.status = 'err';
			test.message = 'Не найдено файлов для копирования';
		}
	}catch(err){
		test.status = 'err';
		test.message = err.message;
	}
	printTestResult( test );
	return test;
}

/* ================================ SMB =================================== */
// SRC

async function smb_src_test0( resource, force ){
	let test = {};
	let flag = false;
	test.name = 'Формальная проверка указанных параметров';
	try{
		if( !resource.dirname ) throw new Error('Не указан ресурс');;
		if( !resource.basename ){
			if( force ){
				resource.basename='*';
				await conf_save();
			}else{
				throw new Error('Не указан объект резервирования');
			};
		}
		if( !resource.user ) throw new Error('Не указан пользователь');
		if( !resource.pass ) throw new Error('Не указан пароль');
		test.status = 'ok';
		if(flag) test.messge = 'Исправлено';
	}catch(err){
		process.stderr.write( err.message + '\n');
		test.status = 'err';
		test.message = err.message;
	}
	printTestResult( test );
	return test;
}

async function smb_src_test1( resource ){
	let test = {};
	test.name = 'Проверка подключения к smb://' + resource.host;
	try{
		let a = resource.dirname.split('/').filter( s=>{ return s; } );
		let share = a.shift();
		let list;
		try{
			list  = await exec('smbclient', [ '\\\\' + resource.host + '\\' + share, resource.pass, '-U', resource.user, '-c', 'pwd' ] );
		}catch(e){
			smb_error(e); 
		}
		if(list[0].match(/^Current directory is/)){
			test.status = 'ok';
		}else{
			test.status = 'err';
			test.message = 'Не удалось подключиться к удалённому серверу';
		}
	}catch(err){
		process.stderr.write( err.message + '\n');
		test.status = 'err';
		test.message = err.message;
	}
	printTestResult( test );
	return test;
}

async function smb_src_test2( resource ){
	let test = {};
	test.name = 'Проверка объектов резервирования';
	try{
		let a = resource.dirname.split('/').filter( s=>{ return s; } );
		let share = a.shift();
		let path  = a.join('/');
		let list;
		try{
			list  = await exec('smbclient',  [ '\\\\' + resource.host + '\\' + share, resource.pass, '-U', resource.user, '-c', 'prompt OFF;recurse ON;'+(path?'cd "' + path + '";':'')+'ls ' + resource.basename ]);
		}catch(e){
			smb_error(e);
		}
		if(list.length>0){
			test.status = 'ok';
			test.message = 'Файлов для копирования: ' + list.length;
		}else{
			test.status = 'err';
			test.message = 'Не найдены объекты копирования';
		}
	}catch(err){
		//process.stderr.write( err.message + '\n');
		test.status = 'err';
		test.message = err.message;
	}
	printTestResult( test );
	return test;
}

// DST

async function smb_dst_test0( resource ){
	let test = {};
	test.name = 'Формальная проверка указанных параметров';
	try{
		if( !resource.dirname ) throw new Error('Не указан ресурс');;
		if( !resource.basename ) throw new Error('Не указан объект резервирования');;
		if( !resource.user ) throw new Error('Не указан пользователь');
		if( !resource.pass ) throw new Error('Не указан пароль');
		test.status = 'ok';
	}catch(err){
		process.stderr.write( err.message + '\n');
		test.status = 'err';
		test.message = err.message;
	}
	printTestResult( test );
	return test;
}

async function smb_dst_test1( resource ){
	let test = {};
	test.name = 'Проверка подключения';
	try{
		let a = resource.dirname.split('/').filter( s=>{ return s; } );
		let share = a.shift();
		let list;
		try{
			list  = await exec('smbclient', [ '\\\\' + resource.host + '\\' + share, resource.pass, '-U', resource.user, '-c', 'pwd' ] );
		}catch(e){
			smb_error(e); 
		}
		if(list[0].match(/^Current directory is/)){
			test.status = 'ok';
		}else{
			test.status = 'err';
			test.message = 'Не удалось подключиться к удалённому серверу';
		}
	}catch(err){
		process.stderr.write( err.message + '\n');
		test.status = 'err';
		test.message = err.message;
	}
	printTestResult( test );
	return test;
}

async function smb_dst_test2( resource, force ){
	let test = {};
	test.name = 'Проверка удалённого ресурса на доступность';
	try{
		let a = resource.dirname.split('/').filter( s=>{ return s; } );
		let share = a.shift();
		let path  = a.join('/');
		let list;
		let tmpdir = getRandomString(32);
		try{
			await exec('smbclient', [ '\\\\' + resource.host + '\\' + share, resource.pass, '-U', resource.user, '-c', 'cd "' + path + '"' ]);
		}catch(e){
			if(e.code==1 && force ){
				await exec('smbclient', [ '\\\\' + resource.host + '\\' + share, resource.pass, '-U', resource.user, '-c', 'recurse ON;mkdir "' + path + '"' ]);
				try{
					let statdata = await exec('smbclient', [ '\\\\' + resource.host + '\\' + share, resource.pass, '-U', resource.user, '-c', 'cd "' + path + '"' ]);
				}catch(e){
					smb_error(e);
				}
			}else{
				smb_error(e);
			}
		}
		try{
			list = await exec('smbclient', [ '\\\\' + resource.host + '\\' + share, resource.pass, '-U', resource.user, '-c', 'cd "' + path + '";mkdir ' + tmpdir + ';ls ' + tmpdir + '; rmdir ' + tmpdir ] );
		}catch(e){
			smb_error(e); 
		}
		list = list.filter( l => {
			return l.match( tmpdir );
		});
		if(list.length>0){
			test.status = 'ok';
		}else{
			test.status = 'err';
			test.message = 'Не похоже, что всё в порядке';
		}
	}catch(err){
		//process.stderr.write( err.message + '\n');
		test.status = 'err';
		test.message = err.message;
	}
	printTestResult( test );
	return test;
}

// Вспомогательные функции

function smb_error(e){
	let mess;
	if(e.rows && Array.isArray(e.rows)){
		mess=(e.rows.filter(r=>{
			return r.match(/NT_STATUS/);
		}))[0];
	}else{
		mess = e.message;
	}
	throw new Error(mess);
}

/* ================================ PGSQL =================================== */
async function pgsql_dst_test1(){
	let test = {};
	test.name = 'Проверка режима';
	try{
		test.status = 'err';
		test.message = 'Использование ресурса PGSQL в качестве приёмника не поддерживается';
	}catch(err){
		process.stderr.write( err.message + '\n');
		test.status = 'err';
		test.message = err.message;
	}
	printTestResult( test );
	return test;
}

async function pgsql_src_test1(){
	let test = {};
	test.name = 'Наличие компонентов PstrgeSQL: psql, pg_dump';
	try{
		try{
			let strs = await exec('/usr/bin/which', ['psql']);
			try{
				strs = await exec('/usr/bin/which', ['pg_dump']);
				test.status = 'ok';
			}catch(e){
				test.status = 'err';
				test.message = 'Отсутствует pg_dump';
			}
		}catch(e){
			test.status = 'err';
			test.message = 'Отсутствует psql';
		}
	}catch(err){
		process.stderr.write( err.message + '\n');
		test.status = 'err';
		test.message = err.message;
	}
	printTestResult( test );
	return test;
}

async function pgsql_src_test2( resource, force ){
	let test = {};
	test.name = 'Наличие соответствующей записи в файле .pgpass';
	try{
		await dotpgpass.load();
		test.status = 'ok';
		if(dotpgpass.find(resource.host, resource.port, resource.basename, resource.user)){
			test.status = 'ok';
		}else{
			if( force ){
				dotpgpass.add(resource.host, resource.port, resource.basename, resource.user, resource.pass);
				await dotpgpass.save();
				test.status = 'ok';
			}else{
				test.status = 'err';
				test.message = 'Запись отсутствует';
			}
		}
	}catch(err){
		test.status = 'err';
		test.message = err.message;
	}
	printTestResult( test );
	return test;
}

async function pgsql_src_test3( resource ){
	let test = {};
	test.name = 'Подключение к SQL-серверу и тестовый запрос';
	try{
		let cmd = 'SELECT (27+23) as s;';
		let strs = await exec('psql', ['-h', resource.host, '-p', resource.port, '-U', resource.user, resource.basename, '-c', cmd]);
		if( parseInt(strs[2])==50 ){
			test.status = 'ok';
		}else{
			test.status = 'err';
			test.message = 'Не удалось выполнить тестовый запрос';
		}
	}catch(err){
		test.status = 'err';
		test.message = err.message;
	}
	printTestResult( test );
	return test;
}

async function pgsql_src_test4( resource ){
	let test = {};
	test.name = 'Проверка соответствия версий сервера и pg_dump';
	try{
		let cmd = 'SELECT version() as v;';
		let strs = await exec('psql', ['-h', resource.host, '-p', resource.port, '-U', resource.user, resource.basename, '-c', cmd]);
		if( strs[2] ){
			let lines = await exec('pg_dump', ['-V']);
			let a = lines[0].toString().trim().split(/ /);
			let v_pgdump = a[ a.length - 1 ].split('.').splice(0,2).join('.');
			let m;
			if( m = strs[2].match(/PostgreSQL (\d+\.\d+\.\d+) /) ){
				let v_server = m[1].split('.').splice(0,2).join('.');
				if(v_pgdump == v_server){
					test.status = 'ok';
				}else{
					test.status = 'err';
					test.message = 'Версии не совпадают: ' + v_pgdump + ' != ' + v_server ;
				}
			}else{
				test.status = 'err';
				test.message = 'Не распознанный ответ сервера';
			}
		}else{
			test.status = 'err';
			test.message = 'Не удалось выполнить запрос к серверу';
		}
	}catch(err){
		test.status = 'err';
		test.message = err.message;
	}
	printTestResult( test );
	return test;
}

async function pgsql_src_test5( resource ){
	let test = {};
	test.name = 'Проверка списка схем для резервирования';
	try{
		let cmd = '\\dn';
		let strs = await exec('psql', ['-h', resource.host, '-p', resource.port, '-U', resource.user, resource.basename, '-c', cmd]);
		strs.splice(0, 3);
		strs.splice( strs.length-1, 1);
		strs = strs.map(l =>{
			return l.split('|')[0].trim();
		});
		let strs_index = {};
		strs.forEach(l=>{strs_index[l]=true;});
		if(!resource.dirname){
			test.status = 'ok';
			test.schemas = strs;
			test.message = 'Всего схем для копирования: ' + test.schemas.length;
		}else{
			let a = resource.dirname.split('/').filter( sch => {return sch;});
			let notexist = [];
			for( let sch of a ){
				if( sch in strs_index ){
					continue;
				}else{
					notexist.push(sch);
				}
			}
			if( notexist.length>0 ){
				test.status = 'err';
				test.message = 'Отсутствуют указанные схемы: [' + notexist.join(', ') + ']';
			}else{
				test.status = 'ok';
				test.schemas = a;
				test.message = 'Всего схем для копирования: ' + test.schemas.length;
			}
		}
	}catch(err){
		test.status = 'err';
		test.message = err.message;
	}
	printTestResult( test );
	return test;
}

async function pgsql_src_test6( resource, schemas ){
	let test = {};
	test.name = 'Проверка списка схем на доступность';
	try{
		for(let ch of schemas){
			let cmd = '\\dt ' + ch +'.*';
			let strs = await exec('psql', ['-h', resource.host, '-p', resource.port, '-U', resource.user, resource.basename, '-c', cmd]);
			strs.splice( 0, 3 );
			strs.splice(strs.length-1, 1);
			strs = strs.map(t =>{
				return t.split('|')[1].toString().trim();
			})
			for(let tb of strs){
				await exec('psql', ['-h', resource.host, '-p', resource.port, '-U', resource.user, resource.basename, '-c', 'select * from ' + ch + '.' + tb + ' limit 1;']);
			}
		}
		test.status = 'ok';
	}catch(err){
		test.status = 'err';
		test.message = err.message;
	}
	printTestResult( test );
	return test;
}

async function pgsql_src_test7( resource, schemas ){
	let test = {};
	test.name = 'Тестовая выгрузка схем';
	try{
		let tempdir = '/tmp/' + getRandomString(32);
		await exec('mkdir', ['-p', tempdir]);
		let src_basename = resource.basename.replace(/ /g,'\\ ');
		for(let schema of schemas){
			await exec('pg_dump', ['-h', resource.host, '-p', resource.port, '-n', schema, '-s', '-U', resource.user, src_basename, '-F', 'p', '-f', (tempdir + '/' + schema) + '.sql' ]);
		}
		await exec('rm', ['-r', tempdir]);
		test.status = 'ok';
	}catch(err){
		process.stderr.write( err.message + '\n');
		test.status = 'err';
		test.message = err.message;
	}
	printTestResult( test );
	return test;
}

/**
 * Возвращает случайную строку указанной длины
 * @param  {number} len Желаемая длина строки
 * @return {string}     Случайная строка
 */
function getRandomString(len){ return crypto.randomBytes( (parseInt(len)/2) || 16).toString('hex'); }

function printTestResult(test){
	if(test.status=='ok'){
		process.stdout.write('      ' + ESC + '32m' + ' ✔ ' + ESC + '0m' + ESC + '2m' + test.name + ' ... ' + ESC + '0m' + '[ ' + ESC + '32m' + test.status + ESC + '0m' + ' ] ' + (test.message?' (' + test.message + ')':'' ) + '\n' );
	}else{
		process.stdout.write('      ' + ESC + '31m' + ' - ' + ESC + '0m' + ESC + '2m' + test.name + ' ... ' + ESC + '0m' + '[ ' + ESC + '31m' + test.status + ESC + '0m' + ' ] ( ' + test.message + ' )\n' );
	}
}
