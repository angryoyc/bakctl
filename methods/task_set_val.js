'use strict';
/*
*  добавить объект [-]
*/

const fs = require('fs');
const { promisify } = require('util');
const writeFile = promisify(fs.writeFile);
const values = { 'hourly':true, 'daily':true, 'weekly':true, 'monthly':true };
const getPositions = require ('../modules/getPositions');

module.exports = function ( config ) {
	return async function (iarg, idata) {
		const arg = iarg || {};
		const data = idata || {};
		let conf = require( config.confpath );
		if( ('position' in arg) && ((('crit' in arg) && ('val' in arg)) || ('plan' in arg))){
			let val, crit, plan;
			if('plan' in arg){
				let a = (arg.plan || '24,7,4,12').split(/[.,:|-]/);
				plan = { hourly:parseInt(a[0] || '24'), daily:parseInt(a[1] || '7'), weekly:parseInt(a[2] || '4'), monthly:parseInt(a[3] || '12') }
			}else{
				if( !(parseInt(arg.val)>=0) ) throw new Error('Неверное значение количества');
				val =  parseInt(arg.val);
				if( !(arg.crit in values) ) throw new Error('Неверное значение критерия. Доступны следующие значения: ' + Object.keys(values).join(', ') + '. Указано: ' + arg.crit);
				crit = arg.crit;
			}
			let list = getPositions( conf, arg.position );
			for( let obj of list ){
				if( !obj.plan ) obj.plan = {};
				for(let val in values){
					obj.plan[ val ] = obj.plan[ val ] || 0;
				}
				if( plan ){
					obj.plan = Object.assign({}, plan);
				}else{
					obj.plan[ crit ] = parseInt( val );
				};
			}
			process.stdout.write('Установлено для ' + list.length + ' элементов \n');
			await writeFile( config.confpath, JSON.stringify(conf, null, 4), 'utf8');
			process.stdout.write('сохранено\n');
		}else{
			throw new Error('Необходимо указать позицию или ключевое слово all');
		}
		return;
	};
};
